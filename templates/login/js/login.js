/**login module and login controller*/

(function(){

var login = angular.module('kooletsLogin',[]);
	
login.controller('LoginController', function($log,  Login, UserCredentials, LoggedIn,ProfileDetails, $scope, $rootScope, $location, SideMenuState, DiscoveryEvents, $mdDialog){
	$log.info("Login Controller is loaded");	
		
	var self = this;
	
	self.register = function(){
		$location.path('/register')
	}
	
	self.login = function(ev){
		Login.login(self.email, self.password).then(function(data){
		
					
				if(angular.equals(data.result, "user ok")){
				
				
				
					//sets data to local storage
					UserCredentials.setUserData(data)//store to local storage
					
					//set menu state to true to display navigation
					SideMenuState.setMenuState();
					console.log("You are logged in");
					
					
					DiscoveryEvents.getEvents();
					DiscoveryEvents.setUserLocation();
					
					//broadcast event to update menu state
					$rootScope.$broadcast("ShowNav");
					
					//reload profile url
					$rootScope.$broadcast("reloadUrl");
					
					//change location
					$location.path('/profile');
					
				}else{
				
					$mdDialog.show(
						$mdDialog.alert()
						.parent(angular.element(document.body))
						.content("Invalid username or password")
						.title('Sorry...')
						.ok("Ok")
						.targetEvent(ev)).then(function(){
							self.email = "";
							self.password = "";
						});
					
					
				}
			
			});
		}
	
		LoggedIn.isUserLoggedIn();
	
	});
	
	
/**@param _email and _password
*	
*/
login.factory('Login', function($http, $log, portUser, baseUrl, userLogin){
	
	var LoginService = {};
	
	LoginService.login = function(_email, _password){
		
		var hash = CryptoJS.SHA3(_password, { outputLength: 224 });
		
		var hashPassword = ""+hash;
		var email = _email;
		
		$log.info(hashPassword, email);
		
		
		return $http.post(baseUrl + portUser + userLogin, {"email" : email, "password": hashPassword}).then(function(result){
			$log.info("Login Result", result.data);
			return result.data;
		});
	
	};
	return LoginService;
});



	
})()