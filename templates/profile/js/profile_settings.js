(function(){

var profile = angular.module('ProfileSettings', ['ImageCropper']);

profile.controller('ProfileSettingController', function(Date, Month, Year, $mdDialog,localStorageService,UserDetail, $log,BirthDateSplitter,UpdateUser,BirthDateConcatenate,ProfilePhoto, Upload,$scope,baseUrl , portUser , userUploadPic){
	
	var self  = this;
	
	self.month = Month;
	self.date = Date;
	self.year = Year;
	
	self.profile_url = ProfilePhoto.getUrl();
	self.user_id = localStorageService.get("koolets_user");
	
	UserDetail.getUserDetails().then(function(data){
		$log.info("Profile Setting", data);
		
		BirthDateSplitter.splitDates(data.koolets_user[0].birthdate);
		
		self.FORM = data.koolets_user[0];
		self.birthYear = parseInt(BirthDateSplitter.getYear());
		self.birthMonth = parseInt(BirthDateSplitter.getMonth());
		self.birthDate = parseInt(BirthDateSplitter.getDay());
		$scope.FORM = data.koolets_user[0];
	});
	
	
	self.saveChanges = function(){
			self.FORM.birthdate = BirthDateConcatenate.combinedBirthdates(self.birthYear, self.birthMonth, self.birthDate);
			
			$log.info("Profile setting edit form", self.FORM);
			UpdateUser.updateUser(self.FORM).then(function(data){
				$mdDialog.show(
					$mdDialog.alert()
							.title('Profile Setting')
							.content('Profile changes has been saved.')
							.ok('Ok')
				)
			});
	}
	
	
	self.uploadProfilePhoto = function(){
		$mdDialog.show({
				controller : 'ProfileSettingController',
				templateUrl : 'templates/profile/partials/profile_upload.html',
				parent : angular.element(document.body)
			});
	}
	
	
	
	function dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}



var fd = new FormData();
$scope.$on("croppedDataImage",function(event,data){
				console.log(data.dataUri, data.files);

			
				$scope.blob = dataURLtoBlob(data.dataUri);

				
				 	fd.append("file", $scope.blob, data.files);
					console.log($scope.blob);
					$scope.file = data.files;
					 $scope.uploadProfile();
		
			});


			var file_ProfileBanner;
			
			$scope.imageIcon = true;
			$scope.newProfile= false;

			$scope.$watch('profilePicture', function(){
					$scope.currentProfile= true;
					$scope.profilePicChange($scope.profilePicture);
				});


				$scope.profilePicChange=function(files){

					if(files && files.length){

					
						file_ProfileBanner = files[0];
						$scope.imageIcon = false;
						$scope.newProfile= true;
						
					}else{
						
					}
				}

			$scope.imageIcon = true;
			$scope.uploadProfile = function(){
				console.log("upload Profile picture");
			Upload.upload({
					url: baseUrl + portUser + userUploadPic,
					fields : {'idkoolets_user' : localStorageService.get('koolets_user')},
					// data : {data : $scope.blob},
					file :  $scope.blob //file_ProfileBanner

				}).progress(function(evt){
				    $scope.progressUpload =  parseInt(100.0 * evt.loaded / evt.total);
				}).success(function(data, status, headers, config){
						console.log(data);
						console.log("Success Upload");
						$scope.$emit('reloadProfileImage');
						
				}).error(function(data,status, headers, config){
					console.log("Error Upload");
					
				});
		
					}
	
	
	$scope.$on("UpdateUserPassword", function(){
		self.saveChanges();
	});
	
});


profile.directive('userAccount', function(){
	return{
		restrict : 'EACM',
		template :  '  <div class="panel-body"><div layout layout-sm="column">' +
                     '         <md-input-container flex>' + 
                      '            <label >Email</label>' +
                      '            <input ng-model="profileSetting.FORM.email" required >' +
                      '        </md-input-container>' +
                      '    </div>' +
					  '<md-input-container>' +
					  '<label>Current password</label>' +
					  '<input type="password" ng-model="myPass"  />'+
					  '</md-input-container>' +
                      '    <div layout layout-sm="column">' +
                      '        <md-input-container flex>' +
                      '            <label >New Password</label>' +
                      '            <input ng-model="password" required type="password" id="passwordRetype">' +
                      '        </md-input-container>' +
					'		  <md-input-container flex>' +
                     '             <label >Re-type Password</label>' +
                     '             <input ng-model="retype" required type="password" id="passwordRetyped">' +
                     '         </md-input-container>' +
                     '     </div> </div> <footer class="text-right">' +
					 '   <md-button class="md-raised"   id="custom-button-1" ng-click="savePassword()"> ' + 
					'		<span id="button-label-w"><i class="fa fa-floppy-o "></i>&nbspSave Changes</span> ' +
					'	</md-button>' +
					' </footer>',
		controller : 'ProfileAccountController'
	}
	
	
	
});



profile.controller('ProfileAccountController',function($mdDialog,$scope, checkUserPassword,$log){
		
			$scope.FORM = {};
			var userPass = $scope.myPass;
		
				
			
				function passwordCheckEquality(){
				if(angular.equals($scope.password, $scope.retype)){
					
					angular.element(document.getElementById("passwordRetype").style.border="0px solid red");
					angular.element(document.getElementById("passwordRetyped").style.border="0px solid red");
					
					passwordUserCheck()
					
				}else{
				
					angular.element(document.getElementById("passwordRetype").style.border="2px solid red");
					angular.element(document.getElementById("passwordRetyped").style.border="2px solid red");
				}
			}
			
			
			function passwordUserCheck(){
				
					var hash = CryptoJS.SHA3($scope.myPass,{outputLength:224});
						var hashedP = ""+hash;
					$log.info(hashedP);
					 checkUserPassword.checkPass(hashedP).then(function(data){
						$log.info(data);
						if(angular.equals(data.result,  "ok")){
							updatePassword($scope.retype);
						}
					 })
			
			}
			
			
			function updatePassword(_pass){
				
				var hashed = CryptoJS.SHA3(_pass,{outputLength:224});
				var hashedPassword = ""+hashed;
				
				$log.info("password hash", hashedPassword);
				
				console.log("FORMDETAIL", $scope.FORM);
				$scope.FORM.password = hashedPassword;
				$scope.$emit("UpdateUserPassword");
			}
		
			$scope.cancel = function(){
			
				$mdDialog.hide();
			}
			
			$scope.ok = function(_v){
			
				$mdDialog.hide()
			
			}
		
			$scope.savePassword = function(){
				passwordCheckEquality();
				console.log("save password called.");
			}
			
		
		});


})()