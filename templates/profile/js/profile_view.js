(function(){
	
	var profile = angular.module('ProfileView', []);
	
	profile.controller('ProfileViewController', function($log, $location, ProfilePhoto, UserDetail, localStorageService, UserCreatedPod,PodBanner, AllEvents,  EventPhoto, ProfileDetail, $route, QrCodeUrl, BirthDateSplitter, monthValue, $rootScope){
		
		
		
		
			//reload profile url
					$rootScope.$broadcast("reloadUrl");
		
		
		var self = this;
		self.eventCount = 0;
		self.podCount = 0;
		
		
		$log.info("ProfileViewController is loaded.")
		
		$log.info($route.current.params._id);
		
		self.user_id  = $route.current.params._id;
		
		self.photo_url = ProfilePhoto.getUrl();
		
		$log.info("QRCODE URL profile_view", QrCodeUrl.getUrl());
		
		
		self.userQr = QrCodeUrl.getUrl();
		
		
		ProfileDetail.getProfile($route.current.params._id).then(function(data){
			
			$log.info("User profile", data.koolets_user[0]);
			
			self.profile = data.koolets_user[0];
			
			BirthDateSplitter.splitDates(data.koolets_user[0].birthdate);
			var birthMonth = BirthDateSplitter.getMonth();
				self.birthday = monthValue.switchMonth(parseInt(birthMonth)) + " " + BirthDateSplitter.getDay() + ", " + BirthDateSplitter.getYear();
		});
		
		

		
		


			//events
			self.eventBanner = EventPhoto.getEventPhotoUrl();
		
			AllEvents.getEvents().then(function(data){
		
			$log.info("profile public events", data);
			self.events = data;
			
			
		});
		
		
			//pods
		self.urlBanner = PodBanner.getBannerUrl();
		UserCreatedPod.getCreatedPod().then(function(data){
			
			$log.info("Created Pods", data);
			
			self.created = data;
			
		});
		
		
			//counter event
			self.eventCounter = function(){
	
				self.eventCount++;
			}
			
			
		//counter pod
		self.podCounter = function(){
	
			self.podCount++;
		
			}
		
	});
	
	
			profile.directive('photoProfile', function(){
		return{
			restrict : 'EACM',
			template : '<div  class="bg-success" style="border:0px solid none">' +
                  '<div class="panel-body">'+
                  '  <div class="clearfix text-center m-t">'+
                  '    <div class="inline">'+
                  '      <div class="easypiechart easyPieChart" data-percent="75" data-line-width="5" data-loop="false" data-bar-color="#92cf5c" data-track-color="#f5f5f5" data-scale-color="false" data-size="150" style="width: 150px; height: 150px; line-height: 150px;">'+
                  '        <div class="thumb-lg animated rollIn">'+
                  '          <img ng-src="{{profileView.photo_url}}{{profileView.user_id}}" class=" avatar border l-r ">'+
                  '        </div>'+
                  '      <canvas width="150" height="150"></canvas></div>'+
                  '      <div class="h4 m-t m-b-xs">{{profileView.profile.username}}</div>'+
                 
                  '    </div>          '   +         
                  '  </div>'+
                 ' </div>'+
				 
			'	<div class=" lter text-center ">'+
                '    <div class="row pull-out">'+
                 '     <div class="col-xs-6  " style="background-color: #34495e">'+
                  '      <div class="padder-v">'+
                   '       <span class="m-b-xs h4 block">{{profileView.eventCount}}</span>'+
                  '        <small class="text-muted">Events</small>'+
                  '      </div>'+
                  '    </div>'+
                  '    <div class="col-xs-6" style="background-color: #2c3e50">'+
                    '    <div class="padder-v">'+
                     '     <span class="m-b-xs h4 block">{{profileView.podCount}}</span>'+
                     '     <small class="text-muted">Pods</small>'+
                     '   </div>'+
                   '   </div>'+
                 
               '     </div>'+
              '    </div>'+
			  '<detail-profile></detail-profile>' +
                '</div>'
		}
		
	
	});
	
	profile.directive('detailProfile', function(){
		return{
		
			restrict : 'EACM',
			template: '<div class="bg-white"><div class="panel-body "><br/>     '  +               
                    '  <div class="clearfix m-b">'+
                    '   <i class="fa fa-circle text-warning pull-right m-t-xs"></i>'+
                    '    <a href="#" class="thumb-sm pull-left m-r">'+
                    '      <span class="fa-stack pull-left m-l-xs">'+
                    '        <i class="fa fa-circle text-info fa-stack-2x" style="font-size:2em"></i>'+
                    '        <i class="fa fa-user fa-stack-1x text-white" style="font-size:1em"></i>'+
                    '      </span>'+
                    '    </a>'+
                    '    <div class="clear">'+
                    '      <a href="#" ><strong >{{profileView.profile.fname}} {{profileView.profile.fname}} {{profileView.profile.fnamelname}}</strong></a>'+
                     '   </div>'+
                   '   </div>'+
                   '	<div class="clearfix m-b">'+
                 '      <i class="fa fa-circle text-warning pull-right m-t-xs"></i>'+
               '         <a href="#" class="thumb-sm pull-left m-r">'+
                '          <span class="fa-stack pull-left m-l-xs">'+
                '            <i class="fa fa-circle text-warning fa-stack-2x" style="font-size:2em"></i>'+
                '            <i class="fa fa-birthday-cake fa-stack-1x text-white" style="font-size:1em"></i>'+
                '          </span>'+
                '        </a>'+
                '        <div class="clear">'+
                '          <a href="#" ><strong >{{profileView.birthday}}</strong></a>'+
                '        </div>'+
                '      </div>	'+
			'				<div class="clearfix m-b">'+
            '           <i class="fa fa-circle text-warning pull-right m-t-xs"></i>'+
            '            <a href="#" class="thumb-sm pull-left m-r">'+
            '              <span class="fa-stack pull-left m-l-xs">'+
            '                <i class="fa fa-circle text-success fa-stack-2x" style="font-size:2em"></i>'+
            '                <i class="fa fa-envelope fa-stack-1x text-white" style="font-size:1em"></i>'+
            '              </span>'+
            '            </a>'+
            '            <div class="clear">'+
            '              <a href="#" ><strong >{{profileView.profile.email}}</strong></a>'+
             '           </div>'+
             '         </div>	'+
             '       </div></div>'
			}

	});
	
profile.directive('podsProfile', function(){
	return{
		restrict : 'EACM',
		template :'	<div class="col-md-8 col-md-offset-2">'+
				'	<hr>'+
				'<h4 class="text-success">Pods</h4>'+
				'<div class="row">'+
				'<div class="col-md-3" ng-repeat="pods in profileDetail.created" ng-if="pods.pod_admin == profileView.user_id" >'+
				'	<md-card >'+
			'		<img ng-src="{{profileDetail.urlBanner}}{{pods._id}}" width=150 height=120>'+
			'						<md-card-content>'+
			'					<div layout="row" >'+
		'									<a class="text-xs">{{pods.pod_title}}</a>'+
	'									</div>'+
	'							</md-card-content>'+
	'						</md-card>'+
'					 </div>'+
'					</div>'+
'					</div>'
	}
});	
	
	
 profile.directive('eventsProfile', function(){
	return{
		restrict : 'EACM',
		template: ' <div class="col-md-12">' +
			' <hr>' +
		'	<h4 class="text-success">Events</h4>' +
			'	<div class="col-md-3" ng-repeat="event in profileView.events" ng-if="event.event_admin == profileView.user_id" >' +
			'		<md-card >' +
				'						<img ng-src="{{profileView.eventBanner}}{{event._id}}" width=150 height=120>' +
				'					<md-card-content>' +
					'						<div layout="row" >' +
				'							<a class="text-xs">{{event.event_title}}</a>' +
				'						</div>'+
				'					</md-card-content>' +
				'			</md-card>' +
			' </div>' +
			  ' </div>' 
	}
 });
	
	
})()