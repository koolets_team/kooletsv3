(function(){

var profile = angular.module('ProfileService',[]);


//user profile url
profile.factory('ProfilePhoto', function(baseUrl,userPic, portUser){
	return{
		getUrl : function(){
			var url = baseUrl + portUser + userPic;
			return url;
		}
	}
});


profile.factory('UserDetail', function($http, localStorageService, baseUrl, portUser, userDetails){
	return{
		getUserDetails: function(){

		var koolets_user_id = localStorageService.get('koolets_user');

		return $http.get(baseUrl + portUser + userDetails + koolets_user_id)
				.then(function(_details){
					return _details.data;
			});	
		}
	}
});



profile.factory('ProfileDetail', function($http, localStorageService, baseUrl, portUser, userDetails){
	return{
		getProfile: function(_koolets_user_id){

		return $http.get(baseUrl + portUser + userDetails + _koolets_user_id)
				.then(function(_details){
					return _details.data;
			});	
		}
	}
});


profile.factory('UpdateUser', function($http, baseUrl, portUser, userUpdate, localStorageService){
	return{
		updateUser: function(_form){
			var koolets_user_id = localStorageService.get('koolets_user');
		return $http.post(baseUrl + portUser + userUpdate +  koolets_user_id, _form)
				.then(function(_details){
					return _details.data;
			});	
		}
	}
});


profile.factory('checkUserPassword', function($http, baseUrl, portUser, userCheckPassword,localStorageService){
return{
	checkPass:function(_v){
	return $http.get(baseUrl + portUser  +  userCheckPassword + localStorageService.get('koolets_user') + '/' + _v)
				.then(function(_details){
					return _details.data;
			});		
	   }
    }
});


profile.factory('QrCodeUrl', function(baseUrl,userQR, portUser){
	
	return{
		getUrl : function(){
			var qr = baseUrl  + portUser + userQR;
			 return qr;
		}
	}
	
});


})()