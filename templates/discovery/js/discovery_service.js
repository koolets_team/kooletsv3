(function(){

	var discovery = angular.module('DiscoveryService', []);
	
	
	discovery.factory('DiscoveryEvents', function(AllEvents, $log, NavigatorGeolocation){
	
var factory = {};
		
		var events = "";
		var coordinates = [];
		var UserLocation;
		var titles = [];

			function loadMap() {
		
			
	 
			try{
			
				 var bounds = new google.maps.LatLngBounds();
	
        var mapCanvas = document.getElementById('discoveryMap');

        var mapOptions = {
          center: new google.maps.LatLng(UserLocation.Latitude, UserLocation.Longitude),
          zoom: 4,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
       

	   var map = new google.maps.Map(mapCanvas, mapOptions);
	   var infowindow = new google.maps.InfoWindow()
	
		for(var index = 0; index < coordinates.length; index++ ) {
		 
		
		 var LatitudeLongitude = coordinates[index];
		
			
	
		 var _iconClass = iconSelector(LatitudeLongitude[2]);
		 
		
        var position = new google.maps.LatLng(LatitudeLongitude[3], LatitudeLongitude[4]);
        bounds.extend(position);
        var marker = new MarkerWithLabel({
            position: position,
            map: map,
			icon: {
				 path: google.maps.SymbolPath.CIRCLE,
				scale: 0,
			},
			labelClass : _iconClass
			});
				
			
		var content =  "<img  src= 'http://ec2-52-74-230-113.ap-southeast-1.compute.amazonaws.com:9011/eventsBanner/" + LatitudeLongitude[1] + "/28"+ "'><a href='#/event-details/" + LatitudeLongitude[1]+"'>"+  titles[index] +"</a>" ;   

  

google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
        return function() {
           infowindow.setContent(content);
           infowindow.open(map,marker);
        };
    })(marker,content,infowindow)); 

				
			
		  }
		
		}catch(err){
			
				
				$log.warn("No coordinates found.")
				
			}
      }

	  
		function getTitleIndex(_index){
			
			console.log(_index);
		}
	  
		function iconSelector(_category){
		
			if(angular.equals(_category, "sports")){
			
				var sports = "fa fa-futbol-o fa-2x text-primary center circleSport";
				return  sports;
			}else if(angular.equals(_category, "music")){
				
				var music = "fa fa-music fa-2x text-danger circleMusic ";
				return  music;
			}
		
		}
		
		function setCoordinates(){
		
			for(var i = 0; i < events.length; i++){
				
				coordinates.push( [i , events[i]._id,events[i].event_category, events[i].event_location[0].latitude,events[i].event_location[0].longitude]);
				titles.push(events[i].event_title);
				
			}
			
			loadMap();
		
		}
		
		function printCoordinates(){
			$log.info("Coordinates", coordinates);
		}
		
		
			factory.getEvents = function(){
			AllEvents.getEvents().then(function(data){
				
				events = data;
				setCoordinates();
			
				$log.info("Discovery Events -> Events", events);
			});
			
		}
		
	
		
		factory.loadDiscoveryMap = function(){
		
			
			loadMap();
		}
		
		factory.setUserLocation = function(){
		
				    NavigatorGeolocation.getCurrentPosition().then(function(position){

			UserLocation ={
				Latitude : position.coords.latitude,
				Longitude : position.coords.longitude
			}
   
    });
		
		
		}
		
		
		
		return factory;
	});
})()