(function(){

	var discovery = angular.module('Discovery',[]);
	
	discovery.controller('DiscoveryController', function(DiscoveryEvents,$timeout, $rootScope){
	
			//reload profile url
					$rootScope.$broadcast("reloadUrl");
	
		DiscoveryEvents.getEvents();
		DiscoveryEvents.setUserLocation();
		
		
	
	});
	
})()