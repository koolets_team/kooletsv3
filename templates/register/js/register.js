(function(){
	var register = angular.module('kooletsRegister',[]);
	
	register.controller('RegistrationController', function( $q, $timeout, $log, $scope,CheckFormUser,$mdToast, Date, Month, Year, $location, $mdDialog){
		
		var self = this;

		
		//go to signin page
		self.signin = function(){
			$location.path('/signin');
		}
		
	
		//new member or user registration
		self.registerMe = function(){
			
			if(angular.equals(self.FORM.password, self.FORM.retype_password)){
				$log.info("Password Equal")
				
				CheckFormUser.setSubmitData(self.FORM);
				
			}else{
				$log.warn("Password not equal");
			

			}
		}
		
		//month
		self.month = Month;
		//date			
		self.date = Date;
		
		//year
		self.year = Year;
					
		$scope.$on('SuccessRegistration',function(){
			$log.info("Success, I got the broadcast");
			$mdDialog.show(
				$mdDialog.alert()
						.title("Registration")
						.content("You are successfully registered to koolets.")
						.ok('Ok')
			)
		});
		
	});
	
	
	/**
	* Custom Services for this controller
	*
	* registration factory service
	* @param _form - the form data which is submitted
	* to the controller.
	*
	* before the form is submitted to the server some inspection 
	* has to be made checking if the user has agreed to terms and services,
	* concatenate birthdate in a YYYY-MM-DD format.
	*
	* form is assign to 'User' model -> custom service. to hold the user information
	*/
	register.factory('CheckFormUser', function($log, $q, $timeout, $http, Register, BirthDateConcatenate){
		
		
		var register = {};
		
		register.finalForm = {};
		
		register.rawForm = '';
		
		register.rawBirthDate = '';
		
		//set the _form data into register property
		register.setSubmitData = function(_form){
			this.rawForm = _form;
			this.isCheckedAgreement();
			this.rawBirthDate = BirthDateConcatenate.combinedBirthdates(_form.birthYear, _form.birthMonth, _form.birthDate);
			
		}
		
		//create password hash
		register.hashPassword = function(){
			  var hash = CryptoJS.SHA3(this.rawForm.retype_password, { outputLength: 224 });
				
			return ""+hash;
		}
		
		//if user agrees to the terms and services it will continue to register
		register.isCheckedAgreement = function(){
			if(this.rawForm.agree){
				this.userRegister();
			}
		}
		//assign final values to the form before saving the user
		register.setFinalForm = function(){
			this.finalForm = {
				username : this.rawForm.username,
				fname : this.rawForm.firstname,
				lname : this.rawForm.lastname,
				mname : this.rawForm.middlename,
				birthdate : BirthDateConcatenate.combinedBirthdates(this.rawForm.birthYear, this.rawForm.birthMonth, this.rawForm.birthDate),
				gender : this.rawForm.gender,
				password : this.hashPassword(),
				email : this.rawForm.email
			}
		}
	
		
		register.userRegister = function(){
			$log.info("Yeah ready.");
			this.setFinalForm();
			
			Register.saveUser(this.finalForm);
		}
		
		
		return register;
	});
	

	
	/**
	* form is submitted to the server and verifacation link is sent to your email
	*@param - _form -> final form 
	*/
	register.factory('Register', function($rootScope,  $http,$log, baseUrl, portUser, userCreate){
		return{
			saveUser : function(_form){
				
				$log.info("Final form", _form);
				$http.post(baseUrl + portUser + userCreate, _form).then(function(result){
				
					$log.info("Save user result",result.data);
					
					$rootScope.$broadcast("SuccessRegistration");
					
				});
			}
		}
	});
	
})()