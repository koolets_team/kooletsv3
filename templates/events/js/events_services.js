(function(){

var events = angular.module('EventsServices', []);


/**
* Service for getting all the events
*/
events.factory('AllEvents', function($http, PublicEvents, portEvent, baseUrl, localStorageService, $log){

		return{
			getEvents : function(){
			
				return $http.get(baseUrl + portEvent +  PublicEvents + localStorageService.get("koolets_user")).then(function(result){
					
					
					return result.data;
				});
			
			}
		
		}
	
});


/** service to return event photo url
*/
events.factory('EventPhoto', function(baseUrl, EventImgBanner, portEvent){
	
	return{
		getEventPhotoUrl : function(){
		
			var url = baseUrl + portEvent + EventImgBanner;
		
			return url;
		
		}
	}
	
});

/**
* custom service to get event detail
*/
events.factory('EventDetails', function($http, baseUrl, portEvent, EventDetail){
	return{
		getDetail : function(_id){
			return $http.get(baseUrl + portEvent + EventDetail + _id).then(function(result){
				return result.data;
			});
		}
	}
});



/**factory for utility*/
events.factory('eventsUtility',function($http, baseUrl, portUtils, InfoType, Category, Type, Event, $log){
  var factory= {};



  //--INFOTYPE--/
  factory.getInfoType = function(){
    return $http.get(baseUrl + portUtils + InfoType).then(function(result){
		$log.info("Info Factory", result.data);
      return result.data;
    });
  }
  //--END INFOTYPE--/

  //--CATEGORY--/
  factory.getCategory = function(_infotype){
    return $http.get(baseUrl + portUtils + Category + _infotype).then(function(result){
		$log.info("Category Factory", result.data);
        return result.data;
    });
  }
  //--END CATEGORY--/

  factory.getType = function(_category){
      return $http.get(baseUrl + portUtils + Type + _category).then(function(result){
	  $log.info("Type Factory", result.data);
        return result.data;
      });
  }
  //--EVENT--/
 factory.getEvent = function(_type){
      return $http.get(baseUrl + portUtils + Event + _type).then(function(result){
	  $log.info("Event Factory", result.data);
        return result.data;
      });
  }
  //--END EVENT--/
  return factory;

});


//service for creating new event
events.factory('NewEvent', function($http, $log, baseUrl, portEvent,CreateNewEvent){

 
  var factory= {};
  //create
  factory.createEvent = function(_form){
   return $http.post(baseUrl + portEvent + CreateNewEvent, _form).then(function(result, status, headers, config){
        $log.info(baseUrl + portEvent + CreateNewEvent);
        $log.info("New event created.", result.data);
        return result.data;
       

    }, function(data, status, headers, config){

      $log.error("New event is not created.");

    });
  }
  
  return factory;
});


/** update event
*/
events.factory('EventUpdate', function($http, baseUrl, portEvent, UpdateEvent){

	return{
		updateEvent :  function(_frm, _i){
			return $http.put(baseUrl + portEvent + UpdateEvent + _i, _frm).then(function(result){
				return result.data;
		});
	}
			
  }

});


//join event
events.factory('EventJoin', function($http, baseUrl, portEvent,JoinEvent){

	return{
		joinEvent :  function(event_id, user_id){
			return $http.post(baseUrl + portEvent + JoinEvent, {"event_id": event_id, "idkoolets_user": user_id}).then(function(result){
				return result.data;
				
		});
	}
			
  }

});


//event member
events.factory('EventMember', function($http, baseUrl, portEvent,MemberEvent, localStorageService){

	return{
		getMembers :  function(_id){
			return $http.get(baseUrl + portEvent + MemberEvent +  _id).then(function(result){
				console.log("EventMember" , result.data);
				return result.data;
				
		});
	}
			
  }

});


//get joined event
events.factory('JoinedEvent', function($http, baseUrl, portEvent,EventJoined, localStorageService){

	return{
		getJoined :  function(){
			
			console.log("Joined events", baseUrl + portEvent + EventJoined);
		
			return $http.get(baseUrl + portEvent + EventJoined +  localStorageService.get("koolets_user")).then(function(result){
				return result.data;
				
		});
	}
			
  }

});

//get joined event
events.factory('UnJoinEvent', function($http, baseUrl, portEvent,EventUnjoined, localStorageService){

	return{
		getUnjoined :  function(_eventId){
			
		return $http.delete(baseUrl + portEvent + EventUnjoined + _eventId + '/' + localStorageService.get("koolets_user")).then(function(result){
				return result.data;
				
		});
	}
			
  }

});


events.factory('PrivateEvent', function($http, EventInvite, EventInvitees, EventRemoveInvitee, baseUrl, portEvent){
	return{
		
		addEventInvitee : function(_form){
			
			return $http.post(baseUrl + portEvent + EventInvite, _form).then(function(result){
				return result.data
			});
			
		},
		removeEventInvitee : function(_eid, _id){
			
			return $http.delete(baseUrl + portEvent + EventRemoveInvitee + _eid + '/' + _id).then(function(result){
				return result.data;
			});
		
		},
		fetchEventInvitee : function(_eventId){
			
			return $http.get(baseUrl + portEvent + EventInvitees + _eventId).then(function(result){
			
				return result.data;
			
			});
			
		}
	
	}
});


})()