(function(){
	
	var event = angular.module('DetailEvent', []);
	
	event.controller('EventDetailController', function(EventDetails, $log, $route, $window,EventPhoto, localStorageService, $location,EventJoin,EventMember, $rootScope,BirthDateSplitter, Month, monthValue, PrivateEvent, $scope){
		
		
	
		
			//reload profile url
			$rootScope.$broadcast("reloadUrl");
		
		var self = this;
		
		var splitStartApp;
		
		var splitEndApp;
		
		self.Event = {};
		
		var event_id = $route.current.params._id;
		
		self.evt_min = localStorageService.get("koolets_user");
		
		self.eventUrl = EventPhoto.getEventPhotoUrl();
		$log.info("event url", self.eventUrl);
		
		EventDetails.getDetail(event_id).then(function(data){
			
			
			self.Event  = data[0];
			$log.info("event detail info", data);
			
			//start
			BirthDateSplitter.splitDates(data[0].event_startApp);
			var monthStart = monthValue.switchMonth(parseInt(BirthDateSplitter.getMonth()));
			self.eventStart = monthStart + " " + BirthDateSplitter.getDay() + ", " + BirthDateSplitter.getYear(); 
			
		
			//end
			BirthDateSplitter.splitDates(data[0].event_endApp);
			var monthEnd = monthValue.switchMonth(parseInt(BirthDateSplitter.getMonth()));
			self.eventEnd = monthEnd + " " + BirthDateSplitter.getDay() + ", " + BirthDateSplitter.getYear(); 
			
			
			
		});
		
	
		
	self.event_view = "detail"; //default
	
	self.switchEventView = function(_v){
		$log.info(_v);
		
		self.event_view = _v;
		console.log(self.event_view, "VV");
	}
	
	
	self.mobile_event_view = "detail";
	
	self.switchMobile = function(_v){
		
		$log.info(_v);
		self.mobile_event_view = _v;
	}
		
		
	//route to edit event
	self.edit = function(_id){
		
		$location.path('/event-settings/' + _id);
	}
	
	
	//join event
	self.joinEvent = function(){
		
		$log.info("I am joining event.", event_id);
		EventJoin.joinEvent(event_id, self.evt_min).then(function(data){
		
				$log.info("Success joined event.")
			
		});
	}
	
	//get event members
	EventMember.getMembers(event_id).then(function(data){
		$log.info("Event members here" , data);
		self.event_members = data;
		self.eventMembersCount = data.length; //count
	  });
	  
	  //get invitee private
	  PrivateEvent.fetchEventInvitee(event_id).then(function(data){
			
			self.inviteeCount = data.length;
		
	  });
		
		
			//view members
	self.viewMembers = function(){
		$log.info("view members clicked.");
		$location.path('/event-members/' + event_id);
	}
	
	//view invitees "private pod"
	self.viewInvitees = function(){
		$location.path('/event-private-invitee/' + event_id);
	}
		
		//back
		self.back = function(){
			$window.history.back();
		}
		
		//create
		self.goCreate = function(){
			$log.info("I got create");
			
			
			$location.path("/event-create");

		}
		
		
		
	});	//end controller

	

	
//directive for detail	
event.directive('eventDetailSection', function(){
		return{
			restrict: 'EACM',
			templateUrl : 'templates/events/partials/event-detail.html'
		}
	});
	
//banner
event.directive('eventBannerSection',function(){
	
	return{
		restrict: 'EACM',
		templateUrl : 'templates/events/partials/event-banner.html'
	}
});	

//map
event.directive('eventMapInfo', function(){
	return{
		restrict : 'EACM',
		templateUrl : 'templates/events/partials/event-map.html'
	}
});

event.factory('monthValue', function(){
	return {
		
		switchMonth : function(_month){
			
			var month;
			
			switch(_month){
				
				case 01:
					month = "January";	
					break;
				case 02:
					month = "February";	
					break;
				case 03:
					month = "March";	
					break;
				case 04:
					month = "April";	
					break;
				case 05:
					month = "May";	
					break;
				case 06:
					month = "June";
					break;
				case 07:
					month = "July";
					break;
				case 08:
					month = "August";
					break;
				case 09:
					month = "September";
					break;
				case 10:
					month = "October";
					break;
				case 11:
					month = "November";
					break;
				case 12:
					month = "December";
					break;
					
					
			}
			return month;
			console.log(month);
			
		}
	
	}
});
	
})()