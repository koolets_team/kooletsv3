(function(){

	var events = angular.module('Events', ['EventsServices']);
	

	
	events.controller('EventsController', function($log,AllEvents, EventPhoto, $location, localStorageService,eventsUtility, $scope, $timeout, $rootScope, EventJoin, $mdToast, DiscoveryEvents, JoinedEvent, UnJoinEvent, $mdDialog){
	
		
		//reload profile url
			$rootScope.$broadcast("reloadUrl");
		
		DiscoveryEvents.getEvents();
		DiscoveryEvents.setUserLocation();
		
		
		var self = this;
		
		self.user_id = localStorageService.get("koolets_user");
		self.eventCategory= {};
		self.category = {};
	
		$log.info("Events controller is loaded.");
		
		self.loadEvents = function(){
			
			AllEvents.getEvents().then(function(data){
		
			$log.info("I got public events", data);
			self.events = data;
			});
		
		}
		
		//call
		self.loadEvents();
		
		self.urlBanner = EventPhoto.getEventPhotoUrl();
		
		console.log(self.urlBanner);
		// desktop switch view
		//self.eventDesktop = "allEvent"; // select default view
		
		
		self.switchEvent = function(_v){
		
			self.eventDesktop = _v;
		
		}
		//end desktop switch
		
		
		//join event
		self.joinEvent = function(_evtId){
			$log.info("I am joining event.", _evtId);
			EventJoin.joinEvent(_evtId, self.user_id).then(function(data){
		
				$log.info("Success joined event.")
				$mdToast.show(
							$mdToast.simple()
									.content('You have joined an event.')
									.position("right")
									.hideDelay(2000)
						)
			//call
		self.loadEvents();
			});
		}
		
	
	
	
		$rootScope.$on("reloadEvents",function(){
			self.loadEvents();
		});
		
		
		$rootScope.$on("createEventView", function(){
			$log.info("I got my event");
			self.eventDesktop = "allEvent";
		});
		
		
		self.detail = function(_id){
			
			$log.info("Event Id", _id);
			$location.path('/event-details/' + _id);
		}
		
		
		
		//joined
		self.loadJoinedEvents = function(){
			
				JoinedEvent.getJoined().then(function(data){
			
			$log.info("Joined event", data);
			self.joinedEvents = data;
		   });
		}
		
		self.loadJoinedEvents();//call
		
		//leave unjoin event
		self.unjoinEvent = function(_evid){
			$log.info("_evid", _evid);
		
			
			
			var confirmUnjoin = $mdDialog.confirm()
										 .title('Unjoin pod')
					                     .content('Are you sure you want to leave in this event?')
					                     .ok('Leave')
					                     .cancel('Cancel')
					                     .ariaLabel('leave pod')
					 
		$mdDialog.show(confirmUnjoin).then(function(){
			
				UnJoinEvent.getUnjoined(_evid).then(function(data){
				$log.info("Success unjoined event");
				self.loadJoinedEvents();
			});
			
		}, function(){
		
			$log.info("I canceled");
		
		});
			
			
		}
		
		$scope.eventView = "tile";
		$scope.switchMobileView = function(_view){
			$scope.eventView = _view;
		}
		
		
		$scope.eventDate = function(_ev_start){
			$log.info(new Date(_ev_start).toDateString());
		}
		
	});

	
	
	
events.directive('eventCreateForm', function(){
	return{
		restrict : 'EACM',
		templateUrl : 'templates/events/create_event.html',
		controller: 'EventCreateController',
		controllerAs : 'newEvent'
		
		
	}
});





events.controller('EventCreateController',function($log, $mdDialog,$scope, GeoCoder,NavigatorGeolocation, localStorageService,ExtractLocation, NewEvent, $rootScope, eventsUtility,$rootScope, wildCardSearch_subAdmin, PodSubAdminDetail, Profile, PrivateEvent){
			
					//reload profile url
			$rootScope.$broadcast("reloadUrl");

			
			var self = this;
			var ReverseGeoCode_LatLng = new google.maps.Geocoder();
			var LatLng;
			
		//url
		self.profile_url = Profile.getUserProfilePicture();
			
			//utils
		eventsUtility.getInfoType().then(function(data){
			self.infoType = data;
		});
		
		$scope.$watch('newEvent.eventCategory.infotype', function(newVal, oldVal){
			
			if(angular.equals(newVal, undefined)){
			
			}else{
			
					$scope.INFOTYPE = newVal;
					eventsUtility.getCategory(newVal).then(function(data){
					$log.info("Category", data);
					self.category = data.koolets_category;
					
				});
			}
				
				
		});
		
		
		$scope.$watch('newEvent.eventCategory.category', function(newVal, oldVal){
				if(angular.equals(newVal, undefined)){
				}else{
					$scope.CATEGORY = newVal;
					eventsUtility.getType(newVal).then(function(data){
					$log.info("Type", data);
					self.type = data.koolets_type;
			    	});
				}
		});
		
		$scope.$watch('newEvent.eventCategory.type', function(newVal, oldVal){
				if(angular.equals(newVal, undefined)){
				}else{
				
					$scope.TYPE = newVal;
					eventsUtility.getEvent(newVal).then(function(data){
					$log.info("Event", data);
					self.event = data.koolets_event;
				});
				}
		});
		
		$scope.$watch('newEvent.eventCategory.event', function(newVal, oldVal){
				if(angular.equals(newVal, undefined)){
				}else{
				
					$scope.EVENT = newVal;
					eventsUtility.getEvent(newVal).then(function(data){
					$log.info("Event", data);
					self.event = data.koolets_event;
				});
				}
		});
		//end utils
			
			
			self.eventCategory = {};
			self.eventPrivate = false; //default
			
			self.create = function(){
				
							$scope.EVENT_FORM = {
					event_idbanner : 		"",
					event_dateCreated :  	Date.now(),
					event_startApp : 		self.evtStartApp,
					event_endApp : 			self.evtEndApp,
					event_admin : 			localStorageService.get("koolets_user"),
					event_subadmin : 		"",
					event_location  : 		ExtractLocation.getExtractLocation(),
					event_private  : 		self.eventPrivate,
					event_title : 			self.event_title,
					event_description : 	self.event_description,
					event_category:			$scope.CATEGORY,
					event_event: 			$scope.EVENT,
					event_infoType :		$scope.INFOTYPE,
					event_type:				$scope.TYPE,
					event_appLimit: 		self.event_appLimit,
					event_price :			self.event_price
				};
			
			
			NewEvent.createEvent($scope.EVENT_FORM).then(function(data){
				$mdDialog.show(
				$mdDialog.alert()
					.parent(angular.element(document.body))
					.title("New event")
					.content("You have successfully created an event.")
					.ok("Ok")
					.ariaLabel("new event")
			)
				
				$log.info("Event create details", data.event_id);
				self.evt_id = data.event_id;
				
				self.addInviteesEvent(); //call
				
			});
				
				
				
				$log.info("Event Form", $scope.EVENT_FORM);
			}
			
			
			self.addInviteesEvent = function(){
					
					try{
						
						self.invitee
						for(var index = 0; index < self.invitee.length; index++){
							
							$scope.eventPrivateInvitee = {};
							$scope.eventPrivateInvitee.idkoolets_user = self.invitee[index].idkoolets_user;
							$scope.eventPrivateInvitee.event_id = self.evt_id;
							
							PrivateEvent.addEventInvitee($scope.eventPrivateInvitee).then(function(data){
								
								$log.info("event invitee added ->", data);
							});
							
						}
					
					}catch(err){
					
						$log.warn("No private event invitee added.");
						
					}
					
			}
			
			
			
			
			
			
			self.showDialogLocation = function(ev){
				$mdDialog.show({
					controller: 'EventCreateController',
					template : '<event-location></event-location>',
					parent : angular.element(document.body),
					targetEvent : ev,
					
				}).then(function(_v){
					$log.info("Location sample added.", _v)
				}, function(){
					$log.info("Location is not added.")
				})
			}
			
			
			self.addLocation = function(_v){
				$mdDialog.hide(_v);
			}
			
			self.cancel = function(){
				$mdDialog.cancel();
			}
			
			
			//typeahead search location
			
		
			
$scope.$watch('newEvent.eventLocation', function(newVal,oldVal){
    GeoCoder.geocode({address : newVal}).then(function(data){
      console.log("Location search ", data);
      self.eventTypeahead = data;
    });
});




self.locationSelected = function(_location){
  if(angular.isObject(_location)){
        console.log("location is object");

       // $scope.EVENT_FORM.event_location. = _location;
       console.log("location", _location);

          ExtractLocation.extractLocationDetails(_location); //extract

        return _location.formatted_address;
  }else{
    throw Error;
  }
}
 


	
function ReverseGeo(_Lat, _Long){

        if(angular.isNumber(_Lat) && angular.isNumber(_Long)){

             LatLng = new google.maps.LatLng(_Lat, _Long);
          ReverseGeoCode_LatLng.geocode({'location' : LatLng}, function(result, status){

            if(status == google.maps.GeocoderStatus.OK){
               $log.info("Reverse geocode", result[0]);
                
                ExtractLocation.extractLocationDetails(result[0]);

            }else{
                $log.warn("Reverse geocode no results found.");
            }

          });

        }else{
            throw Error;
        }


}

self.checkInMap = function(){
    $log.info("Check In");
    NavigatorGeolocation.getCurrentPosition().then(function(position){

     
        ReverseGeo(position.coords.latitude,  position.coords.longitude);
   
    });

    
}	


		self.closeLocationDialog = function(){
			$mdDialog.hide();
		}
			
		

	self.privateEvent = function(){
		$log.info("Event private", self.eventPrivate);
		self.eventInviteeContainer = self.eventPrivate;
	}
	
	
			//typeahead INVITEE search
			
	self.invitee= [];
				
	self.inviteeSearch = function(_sub_admin){
	console.log($scope.eventInviteeAdd);
	$scope.$watch('newEvent.eventInviteeAdd', function(){
		self.eventInviteeAdd = self.eventInviteeAdd;
	});

	if(angular.equals(self.eventInviteeAdd, "")){

	}else{
		wildCardSearch_subAdmin.searchUser_wildCard(self.eventInviteeAdd).then(function(data){
		console.log(data.koolets_user);
		self.resultSearched= data.koolets_user;
		console.log(self.resultSearched, "<><>");
		});
	  }
	}	
	
	self.inviteeEvent = function(pods_subadmin){
		self.selectedEventInvitee = pods_subadmin;
		return pods_subadmin.lname + ',' + pods_subadmin.fname + ',' + pods_subadmin.mname;
	}
	
	self.addInvitee=function(){
		
			console.log("event invitee--<",self.selectedEventInvitee)
				PodSubAdminDetail.getUserDetails(self.selectedEventInvitee.idkoolets_user).then(function(data){
					
					$log.info("event invitee--||", data);
					
					self.invitee.push({
										"idkoolets_user" : self.selectedEventInvitee.idkoolets_user,
										"fname" : data.koolets_user[0].fname,
										"mname" : data.koolets_user[0].mname,
										"lname" : data.koolets_user[0].lname
									});
					
					
					
				});
		}
		
		
	self.removeInvitee = function(_index){
		
		self.invitee.splice(_index, 1);
	}

	//typeahead end
	
	
		
});
		
events.factory('ExtractLocation',function(){
var factory = {};

 factory.LocationDetails = {};

factory.extractLocationDetails=	function (_loc){
  if(angular.isObject(_loc)){
      var splitAddress = _loc.formatted_address.split(',');
          
        
                this.LocationDetails.address = _loc.formatted_address,
                this.LocationDetails.country = splitAddress[splitAddress.length - 1],
                this.LocationDetails.latitude = _loc.geometry.location.G,
               this.LocationDetails.longitude = _loc.geometry.location.K,
               this.LocationDetails.place_id = _loc.place_id
          

		
		  
          console.log("LocationDetails", this.LocationDetails);

  }else{
    throw Error;
  }
}

factory.getExtractLocation = function(){
	return this.LocationDetails;
}

return factory;

});

events.directive('eventLocation',function(){
  return{
    restrict :'EACM',
	controller: 'EventCreateController',
	controllerAs : 'newEvent',
    template : '<br/><md-dialog><md-dialog-content>'+
					'<i class="fa fa-times pull-right" ng-click="createEvent.closeLocationDialog()"></i>'+
                    '<header >Event Location</header>'+
                    '<div class="">'+
                      '<form role="form">'+
                      '<div class="form-group">'+
                  
                    '<input type="text" placeholder="Search location" class="form-control" ng-model="newEvent.eventLocation" typeahead="newEvent.locationSelected(location) as location.formatted_address for location in newEvent.eventTypeahead | limitTo:6" typeahead-min-length=3  />'+
                      '</div>'+
                        '<div class="form-group">' +
                        '<a  class="btn btn-white btn-block" ng-click="newEvent.checkInMap()"><i class="fa fa-map-marker text-info pull-left"></i> Check In</a>' +
                        '</div>' +
                        '<map center= "[{{userLatitude}},{{userLongitude}}]" zoom="13" id="kooletsMap">' +
                           '<marker position="[{{userLatitude}}, {{userLongitude}}]" centered="true"></marker>' +
                        '</map>' +
                        '<button type="submit" class="btn btn-sm btn-default pull-right" ng-click="newEvent.addLocation(hello)">Add location</button>' +
						'<button type="submit" class="btn btn-sm btn-default pull-right" ng-click="newEvent.cancel()">cancel</button>' +
                      '</form>' +
                    '</div>' +
                  '<md-dialog-content></md-dialog>'
  }
});

})()