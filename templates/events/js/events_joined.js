(function(){

var events = angular.module('EventsJoined',[]);

events.controller("EventsJoinedController",  function(JoinedEvent, $log, $mdDialog, UnJoinEvent,  EventPhoto, $location,$rootScope){

				//reload profile url
			$rootScope.$broadcast("reloadUrl");

		var self = this;
		
		self.urlBanner = EventPhoto.getEventPhotoUrl();
		
		//joined
		self.loadJoinedEvents = function(){
			
				JoinedEvent.getJoined().then(function(data){
			self.joinedEvents = data;
			$log.info("Joined event", data);
			
		   });
		}
		
		self.loadJoinedEvents();
		
		
		//detail
		self.detail = function(_id){
			
			$log.info("Event Id", _id);
			$location.path('/event-details/' + _id);
		}
		
		
		//leave unjoin event
		self.unjoinEvent = function(_evid){
			$log.info("_evid", _evid);
		
			
			
			var confirmUnjoin = $mdDialog.confirm()
										 .title('Leave event.')
					                     .content('Are you sure you want to leave in this event?')
					                     .ok('Leave')
					                     .cancel('Cancel')
					                     .ariaLabel('leave pod')
					 
		$mdDialog.show(confirmUnjoin).then(function(){
			
				UnJoinEvent.getUnjoined(_evid).then(function(data){
				$log.info("Success unjoined event");
				self.loadJoinedEvents();
			});
			
		}, function(){
		
			$log.info("I canceled");
		
		});
	}
	
});

})()