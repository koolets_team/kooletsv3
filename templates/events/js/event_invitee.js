(function(){
	
	var events = angular.module("PrivateEventInvitee", []);
	
	events.controller('EventPrivateInviteeController', function(PrivateEvent, $log, $route, PodSubAdminDetail, Profile){
	
		var self = this;
		
		self.eventID = $route.current.params._eid;
		
		self.invitee = [];
		
		//url
		self.profile_url = Profile.getUserProfilePicture();
		
		self.loadInvitees = function(){
			PrivateEvent.fetchEventInvitee(self.eventID).then(function(data){
				$log.info("Event invitees here-", data);
				
					self.inviteesID = data;
				
				$log.info("I got  invitees", data);
				try{
				
				for(var index= 0; index < data.length; index++){
					
					PodSubAdminDetail.getUserDetails(data[index].idkoolets_user).then(function(data){
					
						$log.info("invitees private event->>", data);
						self.invitee.push(data.koolets_user[0]);
						
					});
				}
				
			}catch(err){
				
				//
		}
				
	});
			
}
		
		self.loadInvitees(); //call
		
		
		
	
	});

})()