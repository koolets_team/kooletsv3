(function(){
	var event = angular.module('EventSetting', []);
	
	event.controller('EventSettingController',function($log, $route,EventDetails,eventsUtility,$scope, GeoCoder, NavigatorGeolocation, $mdDialog,EventPhoto, portEvent, UploadBanner, Upload, baseUrl,ExtractLocation,EventUpdate, $rootScope, $location){
		
		var self = this;
		var event_id = $route.current.params._id;
		
		
			//reload profile url
			$rootScope.$broadcast("reloadUrl");
		
		var ReverseGeoCode_LatLng = new google.maps.Geocoder();
		var LatLng;
		
		$log.info("edit event id", $route.current.params._id);
		

		self.loadDetails = function(){
		
				EventDetails.getDetail(event_id).then(function(data){
			$log.info("Edit event data", data);
			
			self.editForm = data[0];
			 splitDates(data[0].event_startApp, data[0].event_endApp);
		
			});
			
		}
		
		self.loadDetails(); //load
		
		var banner = EventPhoto.getEventPhotoUrl();
			
			
			self.eventLoadUrl = function(){
				$log.info("I got event load url");
				self.eventUrl = banner + event_id;;
				
			}
		
		self.eventLoadUrl();
		
		function splitDates(_sa,  _ea){
			startAppSplit = _sa.split("T");
			endAppSplit = _ea.split("T");

		self.EVT_STARTAPP = new Date(Date.parse(startAppSplit));
		self.EVT_ENDAPP = new Date(Date.parse(endAppSplit));
	}
		
		
			eventsUtility.getInfoType().then(function(data){
			self.infoType = data;
		});
		
		$scope.$watch('settingEvent.editForm.event_infoType', function(newVal, oldVal){
			
			if(angular.equals(newVal, undefined)){
			
			}else{
			
					$scope.INFOTYPE = newVal;
					eventsUtility.getCategory(newVal).then(function(data){
					$log.info("Category", data);
					self.category = data.koolets_category;
					
				});
			}
				
				
		});
		
		
		$scope.$watch('settingEvent.editForm.event_category', function(newVal, oldVal){
				if(angular.equals(newVal, undefined)){
				}else{
					$scope.CATEGORY = newVal;
					eventsUtility.getType(newVal).then(function(data){
					$log.info("Type", data);
					self.type = data.koolets_type;
			    	});
				}
		});
		
		$scope.$watch('settingEvent.editForm.event_type', function(newVal, oldVal){
				if(angular.equals(newVal, undefined)){
				}else{
				
					$scope.TYPE = newVal;
					eventsUtility.getEvent(newVal).then(function(data){
					$log.info("Event", data);
					self.event = data.koolets_event;
				});
				}
		});
		
		$scope.$watch('settingEvent.editForm.event_event', function(newVal, oldVal){
				if(angular.equals(newVal, undefined)){
				}else{
				
					$scope.EVENT = newVal;
					eventsUtility.getEvent(newVal).then(function(data){
					$log.info("Event", data);
					self.event = data.koolets_event;
				});
				}
		});
		
		
				$scope.$watch('settingEvent.editForm.event_private', function(newVal, oldVal){
					
					if(newVal){
					
						self.inviteeEventPrivate =true;
					
					}else{
					self.inviteeEventPrivate = false;
					}
		});
		
	
	self.eventPrivateInvitee = function(){
		$location.path("/event-invitee-setting/" + event_id);
		//$log.info("eventPrivateInvitee clicked.");
	}
		
		
	self.save = function(){
	
			console.log("sdfsd", typeof ExtractLocation.getExtractLocation());
			if(Object.getOwnPropertyNames(ExtractLocation.getExtractLocation()).length === 0){
			
			
				eventDates();
				$log.info("original", self.editForm);
				saveChanges(self.editForm);
			
			}else{
			
				
				eventDates();
				self.editForm.event_location = ExtractLocation.getExtractLocation();
				console.log("edited location", self.editForm);
				saveChanges(self.editForm);
				
			}
			
		}
		
	//dates of event
	function eventDates(){
	
		self.editForm.event_startApp = self.EVT_STARTAPP;
		self.editForm.event_endApp = self.EVT_ENDAPP;
	}	
		
	//edit service
	function saveChanges(_form){
	
		EventUpdate.updateEvent( _form, event_id).then(function(data){
			$mdDialog.show(
				$mdDialog.alert()
						.title('Saved')
						.content('Event details changes has been saved.')
						.ok('Ok')
						
			)
			self.loadDetails(); //reload
		});
		
	}//end edit service
		
		self.showDialogLocation = function(ev){
				$mdDialog.show({
					controller: 'EventCreateController',
					template : '<event-location></event-location>',
					parent : angular.element(document.body),
					targetEvent : ev,
					
				}).then(function(_v){
					$log.info("Location sample added.", _v)
				}, function(){
					$log.info("Location is not added.")
				})
			}
			
			
			self.addLocation = function(_v){
				$mdDialog.hide(_v);
			}
			
			self.cancel = function(){
				$mdDialog.cancel();
			}
			
			
			//typeahead search location
$scope.$watch('settingEvent.editForm.event_location[0].address', function(newVal,oldVal){
    GeoCoder.geocode({address : newVal}).then(function(data){
      console.log("Location search ", data);
      self.eventTypeahead = data;
    });
});




self.locationSelected = function(_location){
  if(angular.isObject(_location)){
        console.log("location is object");

       // $scope.EVENT_FORM.event_location. = _location;
       console.log("location", _location);

          ExtractLocation.extractLocationDetails(_location); //extract

        return _location.formatted_address;
  }else{
    throw Error;
  }
}
 


	
function ReverseGeo(_Lat, _Long){

        if(angular.isNumber(_Lat) && angular.isNumber(_Long)){

             LatLng = new google.maps.LatLng(_Lat, _Long);
          ReverseGeoCode_LatLng.geocode({'location' : LatLng}, function(result, status){

            if(status == google.maps.GeocoderStatus.OK){
               $log.info("Reverse geocode", result[0]);
                
                ExtractLocation.extractLocationDetails(result[0]);

            }else{
                $log.warn("Reverse geocode no results found.");
            }

          });

        }else{
            throw Error;
        }


}

self.checkInMap = function(){
    $log.info("Check In");
    NavigatorGeolocation.getCurrentPosition().then(function(position){

     
        ReverseGeo(position.coords.latitude,  position.coords.longitude);
   
    });

    
}	
			
			//-- Upload event banner
		var podFile_Banner;
				$scope.podBannerUpload = function(podId){
   
			Upload.upload({
				url:baseUrl + portEvent + UploadBanner + event_id,
				data : event_id,
				file : podFile_Banner
				}).progress(function(evt){
				$scope.progressUpload = parseInt(100.0 * evt.loaded / evt.total);
			}).success(function(data, status, headers, config){
				console.log(data);
				console.log("event banner success");
				self.eventLoadUrl();
				$mdDialog.hide();
				
			
			}).error(function(data, status, headers, config){
				console.log("event banner error");

			})
 		
		}
			$scope.showDefaultPod = true;
			$scope.previewPodPic = false;
				$scope.$watch('podBanner', function(){
					$scope.podBannerChange($scope.podBanner);
				});

		$scope.podBannerChange = function(files){
			if(files && files.length){
				podFile_Banner = files[0];
				$scope.previewPodPic = true;
				$scope.showDefaultPod = false;
				$scope.uploadShow = true;
			}
		}
		
			self.newBannerPhoto = function(ev){
			$mdDialog.show({
				controller : 'EventSettingController',
				templateUrl : 'templates/partials/pic_upload.html',
				
			});
		}
		
		//--end upload event banner
		
		
		$scope.closeUpload = function(){
			$mdDialog.hide();
		}
		
	});
	
	
	
})()