(function(){

	var events = angular.module('EventMember',[]);
	
	events.controller('EventMembersController', function($log, EventMember, $route, PodSubAdminDetail,$location, $scope, Profile){
	
		var self = this;
	
		//url
		self.profile_url = Profile.getUserProfilePicture();
	var event_id = $route.current.params._id;

		self.members =[];
		
		//get event members
	EventMember.getMembers(event_id).then(function(data){
		$log.info("Event members here" , data);
		self.event_members = data;
		self.eventMembersCount = data.length; //count
		self.requestDetail();
	  });
	
	//request details info of member
	self.requestDetail = function(){
			
			try{
				
				for(var index= 0; index < self.event_members.length; index++){
					
					PodSubAdminDetail.getUserDetails(self.event_members[index].idkoolets_user).then(function(data){
					
						$log.info("event members->>", data);
							self.getInfo(
											
											data.koolets_user[0].fname,
											data.koolets_user[0].mname,
											data.koolets_user[0].lname
										);
						
					});
				}
				
				
				
			}catch(err){
				
				$log.debug("Event members index error.");
			}
		
		}
		
		self.getInfo = function( _fn, _mn, _ln){
		
				self.members.push({
								
								"FirstName" : _fn,
								"MiddleName" : _mn,
								"LastName" : _ln
						  });
	
				$log.info("EVT", self.members);
			
		}
		
	
		self.viewProfile = function(_id){
			
			if(angular.isUndefined(_id)){
					
					$log.warn("Empty Id")
				
			}else{
				$log.info("id", _id);
				$location.path('/view-profile/' + _id.idkoolets_user);
			}
			
		}
	
	});
	
	
	
})()