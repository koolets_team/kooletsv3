(function(){
	
	var event = angular.module('EventInviteeSetting', []);
	
event.controller('EventInviteeSettingController', function($log,  $route, Invitee, $mdDialog,PodSubAdminDetail, $scope, Profile, wildCardSearch_subAdmin, PrivateEvent){
	
	
		
		$scope.event_id = $route.current.params._eid;
		
		$scope.invitee = [];
	
		$scope.inviteesID = [];
		//url
		$scope.profile_url = Profile.getUserProfilePicture();
		
				//get invitees
		$scope.getInvitees = function(pod_id){
		
			PrivateEvent.fetchEventInvitee($scope.event_id).then(function(data){
			$scope.inviteesID = data;
				
				$log.info("I got setting  invitees", data);
				try{
				
				for(var index= 0; index < data.length; index++){
					
					PodSubAdminDetail.getUserDetails(data[index].idkoolets_user).then(function(data){
					
						$log.info("invitees setting->>", data);
						$scope.invitee.push(data.koolets_user[0]);
						
					});
				}
				
			}catch(err){
				
				
			}
				
			});
	
			}
		
				$scope.getInvitees($scope.event_id ); //call
				
				
			
				
				//typeahead search
	$scope.subAdminSearch = function(_sub_admin){
	console.log($scope.podAdminAdd);
	$scope.$watch('podAdminAdd', function(){
		$scope.podAdminAdd = $scope.podAdminAdd;
	});

	if(angular.equals($scope.podAdminAdd, "")){

	}else{
		wildCardSearch_subAdmin.searchUser_wildCard($scope.podAdminAdd).then(function(data){
		console.log(data.koolets_user);
		$scope.resultSearched= data.koolets_user;
		console.log($scope.resultSearched, "<><>");
		});
	  }
	}	
	
	$scope.subAdminPod = function(pods_subadmin){
		$scope.selectedPodSubAdmin = pods_subadmin;
		return pods_subadmin.lname + ',' + pods_subadmin.fname + ',' + pods_subadmin.mname;
	}
	
	$scope.addAdmin=function(){
		
			console.log("SubAdmins--<", $scope.selectedPodSubAdmin)
				PodSubAdminDetail.getUserDetails( $scope.selectedPodSubAdmin.idkoolets_user).then(function(data){
					
					$log.info("SubAdmin--||", data);
					
					$scope.invitee.push({
										"idkoolets_user" : $scope.selectedPodSubAdmin.idkoolets_user,
										"fname" : data.koolets_user[0].fname,
										"mname" : data.koolets_user[0].mname,
										"lname" : data.koolets_user[0].lname
									});
					
					$scope.saveInvitee($scope.event_id, $scope.selectedPodSubAdmin.idkoolets_user);
					
				});
		}

	//typeahead end
	
	
		//save
		$scope.saveInvitee = function(_eventId,_id){
			
				$scope.INVITEE_MEMBERS = {};
								$scope.INVITEE_MEMBERS.idkoolets_user = _id;
								$scope.INVITEE_MEMBERS.event_id = _eventId;
								PrivateEvent.addEventInvitee($scope.INVITEE_MEMBERS).then(function(data){
						
							$log.info("Invitee saved...", data);
							
							$scope.inviteesID.push(data);
						});
			
		}
	
	//invitee
		//remove invitee
	$scope.removeInvitee = function(_id, _index){
		
	
		var confirmLeave = $mdDialog.confirm()
										 .title('Remove invitee')
					                     .content('Are you sure you want to remove invitee?')
					                     .ok('Kick')
					                     .cancel('Cancel')
					                     .ariaLabel('leave invitee')
					 
		$mdDialog.show(confirmLeave).then(function(){
			
			$log.info("I got leave");
			
				$log.info("remove invitee clicked", _id);
				PrivateEvent.removeEventInvitee($scope.event_id, _id).then(function(data){
			
					$log.info("You remove invitee.", data);
			});
				
				$scope.inviteesID.splice(_index, 1);
				
				$scope.invitee.splice(_index ,1);
			
		}, function(){
		
			$log.info("I canceled");
		
		});
		
		
		
	}
	
	});
	
	
})()