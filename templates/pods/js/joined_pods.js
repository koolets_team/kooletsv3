(function(){

var pods = angular.module('JoinedPods', ['PodsService']);


pods.controller('JoinedPodController',function(PodBanner, $location, $log,GetJoinedPod, Profile,localStorageService, $rootScope, $mdDialog,UnjoinPod){

	//reload profile url
			$rootScope.$broadcast("reloadUrl");
	
	var self = this;
	
	
	
	self.urlBanner = PodBanner.getBannerUrl();
	
	self.profile_url = Profile.getUserProfilePicture();
	
	self.userid = localStorageService.get("koolets_user");
	
	//route to user created pods
	self.all = function(){
		$location.path('/pods');
	}
	
	//route to user joined pods
	self.myPods = function(){
    	$location.path('/my-pods');
	}
	
	//route to detail pods
	self.details = function(_id){
	
		$location.path('/detail-pod/' + _id);
		$rootScope.$broadcast("toggleJoinButton");
	}
	
	//edit
	self.edit = function(_id){
		
		$location.path('/pod-settings/' + _id);
	}
	
	
	//unjoin a pod
	self.unjoin = function(_id){
	
		var confirmUnjoin = $mdDialog.confirm()
										 .title('Unjoin pod')
					                     .content('Pod will be removed to your joined pods.\n Are you sure you want to unjoin in this pod?')
					                     .ok('Unjoin')
					                     .cancel('Cancel')
					                     .ariaLabel('Unjoin pod')
					 
		$mdDialog.show(confirmUnjoin).then(function(){
			
			$log.info("I got unjoined");
			UnjoinPod.deleteUnjoin(_id).then(function(data){
			
				$log.info("Success unjoined.");
				fetchJoinedPods(); //reload the service
			});
			
		}, function(){
		
			$log.info("I canceled");
		
		});
	
	}
	
	function fetchJoinedPods(){
	
		GetJoinedPod.getJoined().then(function(data){
			$log.info("joined pods", data);
		
			self.joined = data;
		});
	
	}
	
	fetchJoinedPods();
	
});

})();