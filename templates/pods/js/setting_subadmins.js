(function(){
	
	var pods = angular.module('SettingSubadmins', []);
	
	pods.controller('SettingSubadminsController', function(wildCardSearch_subAdmin,podSubminService,$scope,PodsDetails,PodSubAdminDetail, $route, Profile, $log, $mdDialog, UpdatePod){
	
	var self = this;
	var pod_id = $route.current.params._id;
	self.temp_subadmins = [];

			//typeahead search
	$scope.subAdminSearch = function(_sub_admin){
	console.log($scope.podAdminAdd);
	$scope.$watch('podAdminAdd', function(){
		$scope.podAdminAdd = $scope.podAdminAdd;
	});

	if(angular.equals($scope.podAdminAdd, "")){

	}else{
		wildCardSearch_subAdmin.searchUser_wildCard($scope.podAdminAdd).then(function(data){
		console.log(data.koolets_user);
		$scope.resultSearched= data.koolets_user;
		console.log($scope.resultSearched, "<><>");
		});
	  }
	}	
	
	$scope.subAdminPod = function(pods_subadmin){
		$scope.selectedPodSubAdmin = pods_subadmin;
		return pods_subadmin.lname + ',' + pods_subadmin.fname + ',' + pods_subadmin.mname;
	}
	
	$scope.addAdmin=function(){
		
			console.log("SubAdmins--<", $scope.selectedPodSubAdmin)
				PodSubAdminDetail.getUserDetails( $scope.selectedPodSubAdmin.idkoolets_user).then(function(data){
					
					$log.info("SubAdmin--||", data);
					
					$scope.admins.push({
										"idkoolets_user" : $scope.selectedPodSubAdmin.idkoolets_user,
										"fname" : data.koolets_user[0].fname,
										"mname" : data.koolets_user[0].mname,
										"lname" : data.koolets_user[0].lname
									})
				});
		}

	//typeahead end
	
	
		$scope.admins = [];
			//fetch pod details using its id
	PodsDetails.getDetails(pod_id).then(function(data){
		
		$scope.FORM = data;
		$scope.getSubAdminDetails(data[0].pod_subadmin);
		$scope.pod_name = data[0].pod_title; //title
		$scope.podIdBanner = data[0]._id;
	});
	
	//url profile
	$scope.profile_url = Profile.getUserProfilePicture();

	
		//subAdmins

	$scope.getSubAdminDetails = function(_obj){
		
		$scope.subAdmins = _obj; //-> contains user id's
		$log.info("subADmins", $scope.subAdmins);
		try{
		
			
			for(var index= 0; index <= $scope.subAdmins.length; index++){
				
					
				PodSubAdminDetail.getUserDetail_ID($scope.subAdmins[index].idkoolets_user).then(function(data){
					
					$scope.admins.push({
										"idkoolets_user" : data.idkoolets_user,
										"fname" : data.fname,
										"mname" : data.mname,
										"lname" : data.lname
										
									});
					$log.info("SubAdmin-->", $scope.admins);
				});
				
			}
			
		
			
		}catch(err){
		
			$log.info("No subAdmins");
		}
		
	}
	//delete
	self.delete = function(_index){
		$log.info("_index", _index)
		$scope.admins.splice(_index, 1);
	}
	
	
	
		//save changes
		self.saveForm = function(){
		
			$scope.FORM[0].pod_subadmin =$scope.temp_subadmins;
			console.log("POD_SUBADMIN", $scope.FORM);
				UpdatePod.editPod($route.current.params._id, $scope.FORM[0]).then(function(data){
				
				$mdDialog.show(
					$mdDialog.alert()
							.ariaLabel('Pod subadmin')
							.title('Pod Sub-admin.')
							.content('Pod subadmin changes has been saved.')
							.parent(angular.element(document.body))
							.ok('Ok')
				)
				
			});
			
		}
	
//save
	self.save = function(){
			
			$log.info("admins", $scope.admins);
			try{
				$scope.temp_subadmins = [];
				$scope.adminsLength = $scope.admins.length;
				$log.info("ADMINS LENGT", $scope.adminsLength);
				for(var index = 0; index < $scope.adminsLength; index++){
					
					$scope.temp_subadmins.push({"idkoolets_user" : $scope.admins[index].idkoolets_user});
				
				}
			
				$log.info("Final Ids", $scope.temp_subadmins);
				
				self.saveForm();
				
			}catch(err){
			
				$log.warn(err);
			}
		}
		
	

	
	
	});
})()