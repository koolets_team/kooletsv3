(function(){
	
	var pods = angular.module('CreateNewPods',  []);
	
	
	//Controller
	pods.controller('CreateNewPodController', function($log, $scope, $location,localStorageService, $rootScope){
	
	var self = this;
	
	self.back = function(){
	
		$location.path('/my-pods');
	}
	//reload profile url
			$rootScope.$broadcast("reloadUrl");
	
});


pods.controller('NewPodController',function( $log, $scope, localStorageService, GeoCoder ,CreatePod,$mdToast,wildCardSearch_subAdmin,podSubminService, $mdDialog,  ProfilePhoto, podInviteeService, Invitee, $timeout){
				
				$log.info("NewPodController is loaded.");
				
				$scope.checkboxToggle = false; //checkIn default to false 'Enabled'
				$scope.toggleMembership = false;
				$scope.FormPod = {};
				
				$scope.INVITEE_MEMBERS = [];
				
				$scope.FormPod.pod_admin = localStorageService.get("koolets_user");
				$scope.FormPod.pod_subadmin = [];
				$scope.FormPod.pod_membership_enable = false      //default
				$scope.FormPod.pod_membership_duration = 0        //default
				$scope.FormPod.pod_creationDate = Date.now();
				$scope.FormPod.pod_fees = 0                       //default
				$scope.FormPod.pod_membership_expires = false;    //default
				$scope.FormPod.pod_private = false; 			  //default
				$scope.FormPod.pod_banner = null;				  //default
				$scope.FormPod.pod_summary = null;				  //default
				$scope.FormPod.pod_location = {};
				$scope.FormPod.pod_extra = null;
				$scope.FormPod.pod_approval = true;
		
				
				$log.info('createNewPod controller is loaded');
				
			
				$scope.title = "pod title"
				
			

				$scope.create = function(){
				
					$scope.FormPod.pod_extra = $scope.FieldsArray;
				
					$scope.FormPod.pod_extra = $scope.extraFields;
			
					var subminsPods = $scope._subAdmins;
					
				
								//invitees
				
					//subadmins
					try{
						
						for(var i = 0; i < subminsPods.length; i++){

							$scope.FormPod.pod_subadmin.push({"idkoolets_user" : subminsPods[i].idkoolets_user});
						}
					}catch(err){
					
						$scope.FormPod.pod_subadmin = null;
					}
		
			
					
				
					$scope.FormPod.pod_location  = $scope.locationSelected;
					
					console.log($scope.FormPod);
					
					CreatePod.savePod($scope.FormPod).then(function(data){
						$log.info("Result saved pod. Success!!!", data);
						
						$scope.POD_ID = data.pod_id;
						
						$mdToast.show(
							$mdToast.simple()
									.content('Success!. You just created your pod.')
									.position("right")
									.hideDelay(2000)
						);
							
							$timeout(function(){
								
								$scope.INVITEE();
								
								
							},700)
				
					});
					
				}
				
				
				$scope.INVITEE = function(){
				
				
				
				
						var inviteePods = $scope._invitees;
					try{
						
								for(var i = 0; i < inviteePods.length; i++){
								$scope.INVITEE_MEMBERS = {};
								$scope.INVITEE_MEMBERS.idkoolets_user = inviteePods[i].idkoolets_user;
								$scope.INVITEE_MEMBERS.pod_id = $scope.POD_ID;
								Invitee.saveInvitee($scope.INVITEE_MEMBERS).then(function(data){
						
							$log.info("Invitee saved...", data);
						
						});
							
					}
					
					}catch(err){
						
						$log.debug("Invitee caught error.");
					
					}
				
			
				}
				
			console.log($scope.pod_location, "podLocation");
				
			//input search places
			$scope.$watch('pod_location', function(newVal, oldVal){

				if(angular.equals(newVal, "") || angular.equals(newVal, undefined)){
					
					
					$scope.checkboxToggle = false;
					
				}else{
					
					$scope.checkboxToggle = true;
					
				   GeoCoder.geocode({address : newVal}).then(function(data){
					$scope.searchLocations = data;
					console.log(data);
				  });
				}
			});
	
			$scope.locationPodSearch = function(location){

				var splitAddress = location.formatted_address.split(',');

			$scope.locationSelected ={
				"address" : location.formatted_address,
				"place_id" : location.place_id,
				"country" : splitAddress[splitAddress.length - 1],
				"latitude" : location.geometry.location.G,
				"longitude" : location.geometry.location.K
			};
			
			return location.formatted_address;
		}

		
	$scope.searchAdmin = function(){
		$mdDialog.show({
				controller : 'NewPodController',
				templateUrl : 'searchUserTemplate.html',
				parent : angular.element(document.body)
			}).then(function(_v){
					
					$log.info("V", _v);
					
			}, function(){
					
					$log.info("noop");
					
			});
	}	
		
		
	$scope.cancelSearchAdmin = function(){
	
		$mdDialog.hide()
	}	
	
	$scope.okSearchAdmin = function(){
		
		$mdDialog.hide("test");
	
	}
		
		$scope.profileUrl = ProfilePhoto.getUrl();
		
	//typeahead search subadmin
	$scope.subAdminSearch = function(_sub_admin){
	console.log($scope.podAdminAdd);
	$scope.$watch('podAdminAdd', function(){
		$scope.podAdminAdd = $scope.podAdminAdd;
	});

	if(angular.equals($scope.podAdminAdd, "")){

	}else{
		wildCardSearch_subAdmin.searchUser_wildCard($scope.podAdminAdd).then(function(data){
		console.log(data.koolets_user);
		$scope.resultSearched= data.koolets_user;
		console.log($scope.resultSearched, "<><>");
		});
	  }
	}	
	
	$scope.subAdminPod = function(pods_subadmin){
		$scope.selectedPodSubAdmin = pods_subadmin;
		return pods_subadmin.lname + ',' + pods_subadmin.fname + ',' + pods_subadmin.mname;
	}
	
	$scope.addAdmin=function(){
		podSubminService.getSubmins($scope.selectedPodSubAdmin);
		$scope._subAdmins = podSubminService.setSubmins();
		
		$scope.podAdminAdd = "";
		console.log($scope._subAdmins.length);
			
			
			
		
		}
		
		
	
	$scope.removeSubAdmin= function(index){

		$scope._subAdmins.splice(index, 1);
		console.log(index);
		console.log($scope._subAdmins);
		$scope.$watch('_subAdmins', function(){
				$scope._subAdmins = $scope._subAdmins;
		});
		
	}

	//typeahead end sub-admin

	$scope.PodPrivate = function(){
			if($scope.FormPod.pod_private){
				
				$scope.inviteeContainer = true //show
			
			}else{
				$scope.inviteeContainer = false //hide
			}
		}
	
	
	//typehead invitee
	
	$scope._invitees = []; 
	
		$scope.inviteeSearch = function(_sub_admin){
	console.log($scope.podInviteeAdd);
	$scope.$watch('podAdminAdd', function(){
		$scope.podAInviteeAdd = $scope.podInviteeAdd;
	});

	if(angular.equals($scope.podInviteeAdd, "")){

	}else{
		wildCardSearch_subAdmin.searchUser_wildCard($scope.podInviteeAdd).then(function(data){
		console.log(data.koolets_user);
		$scope.resultSearched= data.koolets_user;
		console.log($scope.resultSearched, "<invitee><invitee>");
		});
	  }
	}	
	
	$scope.inviteePod = function(pods_invitee){
		$scope.selectedPodInvitee = pods_invitee;
		return pods_invitee.lname + ',' + pods_invitee.fname + ',' + pods_invitee.mname;
	}
	
	$scope.addInvitee=function(){
		podInviteeService.getInvitees($scope.selectedPodInvitee);
		$scope._invitees = podInviteeService.setInvitees();
		
		$scope.podInviteeAdd = "";
		console.log($scope._invitees.length);
		
		
		}
		
		
	
	$scope.removeInvitee= function(index){

		$scope._invitees.splice(index, 1);
		console.log(index);
		console.log($scope._invitees);
		$scope.$watch('_invitees', function(){
				$scope._invitees = $scope._invitees;
		});
		
	}
	// end typeahead invitee
	
	$scope.extraFields = [];
		$scope.addField = function(){
				
				$scope.extraFields.push({"FieldName": $scope.fieldName, "FieldValue" : null});
		}
	
	
	});
	
//directive search location and check in location
 pods.directive('createNewPod', function(NavigatorGeolocation, $compile){
		return{
			restrict : 'EACM',
			templateUrl: 'templates/pods/create_new_pod.html',
			controller: 'NewPodController',
			link : function(scope, elem, attrs){
					
				var reverse  = new google.maps.Geocoder();
				var latlng;
				
				//checkbox check-in locations
				var checkBoxUIState = {
					
					//disable search input
					disableSearchInput : function(){
						angular.element(document.getElementById('location').disabled = true);
					},
					//enable search input
					enableSearchInput : function(){
						angular.element(document.getElementById('location').disabled = false);
					}
				};
				
				
				//check in state
				scope.checkInStatus = function(){
					
					if(angular.equals(scope.checkInState, true)){
					
						checkBoxUIState.disableSearchInput();
						
						NavigatorGeolocation.getCurrentPosition().then(function(position){
				
						latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				
						reverse.geocode({'location' : latlng},function(result, status){
				
							if(angular.equals(status, google.maps.GeocoderStatus.OK)){
						
								var splitAddress = result[0].formatted_address.split(',');

							scope.locationSelected ={
									"address" : result[0].formatted_address,
									"place_id" : result[0].place_id,
									"country" : splitAddress[splitAddress.length - 1],
									"latitude" : result[0].geometry.location.G,
									"longitude" : result[0].geometry.location.K
								};
							
									console.log("adasdfkljashdklf", scope.locationSelected)
							}	
				
						});
				
					});
						
				}else{
						checkBoxUIState.enableSearchInput();
					}
					
				};
				
				
				//membership status
				scope.membershipEnableStatus = function(){
					
					if(angular.equals(scope.FormPod.pod_membership_enable, true)){
					
						
						scope.toggleMembership = true;
					
					}else{
						scope.toggleMembership = false;
					}
				
				}
				
				
			}
		}
	 });
	 

	 
	 
})();