(function(){
	var pods = angular.module('PodMembers', []);
	
	pods.controller('PodMemberController', function(Members, $route, $log,PodSubAdminDetail, $scope,Profile,  $location,PodsDetails, localStorageService, $compile, UnjoinPod, $mdDialog){
	
		var self = this;
		var pod_id = $route.current.params._id;
	
		var pod_subadmin; 
		
		$scope.user_ID = localStorageService.get("koolets_user");
		
		//url
		$scope.profile_url = Profile.getUserProfilePicture();
		
		var ids = [];
		
		
				$scope.CHECKADMIN = function(){
				
					console.log("i got checking admin");
					if($scope.user_ID  == $scope.pod_admin){
					
						console.log("you are the admin in this pod");
		angular.element(document.getElementById('members')).append($compile("<pod-members-sub></pod-members-sub>")($scope));
		
					}else{
						
						console.log("you are not admin in this pod");
						//angular.element(document.getElementById('members')).append("<h1>you are not subadmin /admin</h1>");
			angular.element(document.getElementById('members')).append($compile('<pod-members-list></pod-members-list>')($scope));
					}
				
			}
		
		
		
		PodsDetails.getDetails(pod_id).then(function(data){
			$log.info("members pod -> detail", data);
			
			$scope.pod_admin = data[0].pod_admin;
			
			pod_subadmin = data[0].pod_subadmin;
			$log.info("pod_subadmin members pods", pod_subadmin);
			
			try{
				
				for(var index = 0; index < pod_subadmin.length; index++){
			
					ids.push(pod_subadmin[index].idkoolets_user);
				}
				
				console.log("IDS", ids);
			
				
			
			}catch(err){
				
				$log.warn("No members", err);
			
			}
			
			
				var index = $.inArray($scope.user_ID, ids);
				console.log(index)
				if(index < ids.length && index != -1){
					
					console.log("you are pod-subadmin");
				angular.element(document.getElementById('members')).append($compile("<pod-members-sub></pod-members-sub>")($scope));
				
				}else{
					
					console.log("you are not pod-subadmin");
					$scope.CHECKADMIN();
			
				}
	
		});
		
		
	
	
		$scope.getMembers =function(){
			
			
				Members.getMembers(pod_id).then(function(data){
			
			$log.info("Members pod ->", data);
			
			$scope.membersPod(data);
			
			});
			
		
		}
			$scope.members = [];
		
		$scope.membersPod = function(_obj){
			
			$scope.members_list = _obj;
			$log.info("Members_list", $scope.members_list);
			try{
				
				for(var index= 0; index <= $scope.members_list.length; index++){
					
					PodSubAdminDetail.getUserDetails($scope.members_list[index].idkoolets_user).then(function(data){
					
						$log.info("members->>", data.koolets_user[0].idkoolets_user);
						$log.info("members->>>",_obj);
						var temporaryData = containsData(data.koolets_user[0].idkoolets_user,_obj);
						data.koolets_user[0].pod_extra = temporaryData.pod_extra;
						data.koolets_user[0].pod_date_joined =temporaryData.pod_date_joined;
						$log.info("members->>>>",data.koolets_user[0]);
						$scope.members.push(data.koolets_user[0]);
						
					});
				}
				
			}catch(err){
				console.log("members",err)
				
			}
			
			
		}
		
		var containsData = function (searchKey, searchItems){
			var dataToReturn = null;
			$scope.searchList = searchItems;
			console.log("searchKey "+searchKey);
			
			  for ( var i in $scope.searchList){
				  console.log("searchList "+$scope.searchList[i].idkoolets_user)
				  if(searchKey == $scope.searchList[i].idkoolets_user){
					  console.log("Match is found");
					  dataToReturn = $scope.searchList[i];
				  }
			  }
			  return dataToReturn;
		}
		
			$scope.getMembers(); //call
	
		$scope.viewProfileDetail = function(_id){
			
			$location.path("/view-profile/" + _id);
		}
		
		//remove or delete member if you are subadmin or admin in pod
		$scope.removeMember = function(_user_id, _index){
			
			
			
	    //var removeIndex = $scope.members.indexOf(_user_id);
			
	    $log.info("Pod member remove clicked.",_index);
			
				
		var confirmLeave = $mdDialog.confirm()
										 .title('Remove member')
					                     .content('Are you sure you want to remove member?')
					                     .ok('Kick')
					                     .cancel('Cancel')
					                     .ariaLabel('leave invitee')
					 
		$mdDialog.show(confirmLeave).then(function(){
			
			$log.info("I got leave");
			
				$log.info("remove invitee clicked", _user_id);
					UnjoinPod.deleteMember(pod_id, _user_id).then(function(data){
				$log.info("Pod member deleted.");
				$scope.members.splice(_index, 1);
			});
				
			
		}, function(){
		
			$log.info("I canceled");
		
		});
			
		}
		
	
	});
	
	pods.directive('podMembersSub', function(){
		return{
			restrict : 'EACM',
			templateUrl : "templates/pods/partials/pod_memberssub.html"
			
		}
	});
	
	pods.directive("th", function(){
		return{
			restrict : 'EACM',
			template : '<h1>{{members}}</h1>'
		}
	});
	
		pods.directive('podMembersList', function(){
		return{
			restrict : 'EACM',
			templateUrl: "templates/pods/partials/pod_memberslist.html"
			
			
		}
	});
	
})()