(function(){

	var pods = angular.module('CreatedPods', []);
	
	pods.controller('CreatedPodController', function($location, $log,UserCreatedPod,PodBanner, Profile, localStorageService,$timeout, $mdDialog, $rootScope){
	
		//reload profile url
			$rootScope.$broadcast("reloadUrl");
	
		var self = this;
		
		self.urlBanner = PodBanner.getBannerUrl();
		
		//profile url
		self.profile_url = Profile.getUserProfilePicture();
		
		self.userid = localStorageService.get("koolets_user");
		
		UserCreatedPod.getCreatedPod().then(function(data){
			
			$log.info("Created Pods", data);
			
			self.created = data;
		});
		
		//route to all available pods
		self.all = function(){
			$location.path('/pods');
		}
	
		//route to user joined pod
		self.joinedPod = function(){
			$location.path('/joined-pods');
		}
		
		//create new pod
		self.createNew = function(){
			$location.path('/create-new');
		}
		
		//route to details
		self.details = function(_id){

			$location.path('/detail-pod/' + _id);
		
		}
		
		//route to edit or update pod 
		self.edit = function(_id){
			
			$location.path('/pod-settings/' + _id);
		}
		
		
	});

})()