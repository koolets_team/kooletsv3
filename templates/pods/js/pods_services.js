(function(){
	var pods = angular.module('PodsService', []);

	
	/** fetch all public pods available for user
	*@param result - is the result coming from the database
	*/
	pods.factory('PublicPods', function(baseUrl, portPods, AllPods,localStorageService, $log, $http){
		return{
			getPublicPod: function(){
				return $http.get(baseUrl + portPods + AllPods + localStorageService.get('koolets_user')).then(function(result){
				
					return result.data;
				});
			}
		}
	});
	
	
	
	/** url provider for getting the pod banner
	* returns url
	*/
	pods.factory('PodBanner',  function(PodPicture, portPods, PodPicture, baseUrl){
		return{
			getBannerUrl : function(){
				
				var url = baseUrl + portPods + PodPicture;
				return url;
			}
		}
	});
	
	/**
	* custom service to return the pod's detail
	* @params - PodID - the id of the pod
	*/
	pods.factory('PodsDetails', function($http, baseUrl, portPods, PodID){
	return{
		getDetails: function(id){
			console.log("pods_id", id);
			return $http.get(baseUrl+ portPods + PodID + id)
						.then(function(_details){
							//console.log(_details.data);
							return _details.data;
							
					});
				}
			}
		});
		
	/** service for joining a selected pod
	* @params pod_id - the id of pod you want to join
	* @params 'object' - sets the user id
	*/	
	pods.factory('JoinPod', function($http, baseUrl, portPods, PodJoin, localStorageService ,$timeout){
	return{
		podJoin: function(_podId, _requiredFields, _pod_approval){

			return $http.post(baseUrl + portPods + PodJoin ,{idkoolets_user : localStorageService.get('koolets_user'), pod_id : _podId, pod_extra : _requiredFields, pod_status : _pod_approval})
						.then(function(_result){
							//console.log(_details.data);
							return _result;
							
					});
				}
			 }
		});
	
	
	/**
	* service for unjoining a pod
	* removes the pod to your account
	*/
	pods.factory('UnjoinPod', function($http, baseUrl, portPods, PodUnjoin,localStorageService){
	return{
		deleteUnjoin: function(id){
			
			return $http.delete(baseUrl + portPods + PodUnjoin + id +'/'+ localStorageService.get('koolets_user'))
						.then(function(_result){
							//console.log(_details.data);
							return _result.data;
							
					});
				},
		deleteMember : function(_id, _userid){
				
			return $http.delete(baseUrl + portPods + PodUnjoin + _id +'/'+ _userid)
						.then(function(_result){
							//console.log(_details.data);
							
							console.log("Member deleted.");
							return _result.data;
							
					});
			
		}
	}
 });
 
 /**
 * service to fetch all user joined pods
 */
 pods.factory('GetJoinedPod', function($http, baseUrl,portPods, PodGetJoin, localStorageService ,$timeout){
	return{
		getJoined: function(){

			return $http.get(baseUrl + portPods + PodGetJoin + localStorageService.get('koolets_user'))
						.then(function(_result){
							//console.log(_details.data);
							return _result.data;
							
					});
				}
	      }
    });
	
	/**
	* service to fetch all pods and is filtered using angular built in filter
	*/
	pods.factory('UserCreatedPod', function(baseUrl, portPods, AllPods,localStorageService, $log, $http){
		return{
			getCreatedPod: function(){
				return $http.get(baseUrl + portPods + AllPods + localStorageService.get('koolets_user')).then(function(result){
				
					return result.data;
				});
			}
		}
	});
	
	/**
	* service to get members of the pod
	*/
	pods.factory('Members', function($http, baseUrl,portPods, PodMembers){
	return{
		getMembers: function(_podId){

			return $http.get(baseUrl + portPods + PodMembers + _podId)
						.then(function(_result){
							console.log(_result, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<');
							return _result.data;
							
					});
				}
	}
});
	/**
	* service for creating a new pod
	* @params -_form - form details of the pod to be saved.
	*/
 	pods.factory('CreatePod', function($http, baseUrl,portPods,InsertPods){
	return{
		savePod: function(_form){

			return $http.post(baseUrl + portPods + InsertPods, _form)
						.then(function(_result){
							console.log("Success!!!.. pod saved...");
							return _result.data;
							
					});
				}
	}
});



//invitee
 	pods.factory('Invitee', function($http, baseUrl,portPods,PodInvite, $log, PodInvitees, PodRemoveInvite){
	return{
		saveInvitee: function(_form){

			return $http.post(baseUrl + portPods + PodInvite, _form)
						.then(function(_result){
							$log.info("Success!!!.. invitee...");
							return _result.data;
							
					});
				},
				
		getInvitee: function(_pid){

			return $http.get(baseUrl + portPods + PodInvitees + _pid)
						.then(function(_result){
							$log.info("Pod get invitee");
							$log.info("Get Invitee Route", baseUrl + portPods + PodInvitees + _pid);
							return _result.data;
							
					});
				},
				
		removeInvitee : function(_pid, _uid){
		return $http.delete(baseUrl + portPods + PodRemoveInvite + _pid + '/'+ _uid)
						.then(function(_result){
							$log.info("Remove invitee url", baseUrl + portPods + PodRemoveInvite + _pid + '/'+ _uid)
							$log.info("Remove invitee");
						
							return _result.data;
							
					});
			
		
		}
	}
});




/**
* Service to update the created pod
* @params _form -the form that is edited
* @params _id - pod id to be updated
*/
pods.factory('UpdatePod', function($http, baseUrl, portPods, PodUpdate){
	return{
		editPod : function(_id, _form){
			return $http.put(baseUrl + portPods + PodUpdate + _id, _form).then(function(_result){
			
				console.log("Pod edit saved!!!...");
				return _result.data;
			});
		}
	}
});
 
 
pods.factory('wildCardSearch_subAdmin', function($http, baseUrl, portUser, userSearch, $timeout){
	return{
		searchUser_wildCard: function(_searchWord){
			
			return $http.get(baseUrl + portUser + userSearch + _searchWord)
						.then(function(_resultSearch){
							//console.log(_details.data);
							return _resultSearch.data;
							
					});
				}
	}
});


pods.factory('podSubminService', function(){
	return {
		subminPods: [],
		getSubmins: function(_user){
			console.log(_user, "_user");
			this.subminPods.push(_user);
		},
		setSubmins : function(){
			console.log(this.subminPods);
			return this.subminPods;
		}
	}
});


pods.factory('podInviteeService', function(){
	return {
		inviteePods: [],
		getInvitees: function(_user){
			console.log(_user, "_user");
			this.inviteePods.push(_user);
		},
		setInvitees : function(){
			console.log(this.inviteePods);
			return this.inviteePods;
		}
	}
});


pods.factory('PodSubAdminDetail', function($http,  baseUrl, portUser, userDetails){
	return{
		getUserDetails: function(_id){

		return $http.get(baseUrl + portUser + userDetails + _id)
				.then(function(_details){
					return _details.data;
			});	
		},
		getUserDetail_ID: function(_id){
			return $http.get(baseUrl + portUser + userDetails + _id)
				.then(function(_details){
					
					var result = {
							idkoolets_user : _id,
							fname : _details.data.koolets_user[0].fname,
							mname : _details.data.koolets_user[0].mname,
							lname : _details.data.koolets_user[0].lname
							
						
							
						}
					
						return result;
					
			});	
		}
	}
});


pods.factory('PendingMembers', function($http, baseUrl, portPods, PendingPodMembers, $log, PendingApproveMember){
	
	return {
		
		getPending   : function(_id){
			return $http.get(baseUrl + portPods + PendingPodMembers + _id).then(function(result){
				$log.info("Service pending members", result.data);
				return result.data;
			});
		},
		approvePending : function(_id, _form){
		
			return $http.put(baseUrl + portPods + PendingApproveMember + _id, _form).then(function(result){
				$log.info("PENDING APPROVAL SUCCESS..");
				return result.data;
			});
		}
	}
});
 
})()