(function(){

var pods = angular.module('PodApproval', []);

pods.controller('PodApprovalsController', function($route, PodsDetails, $scope, $log, localStorageService, Members, PodSubAdminDetail, $compile, PendingMembers, Profile, UnjoinPod){
		
	$scope.POD_ID = $route.current.params._id;

	$scope.USER= localStorageService.get("koolets_user");
	
	$scope.MEMBER_APPROVAL = [];
	
	
			//url
		$scope.profile_url = Profile.getUserProfilePicture();
	
	
	$scope.IDS = [];
	
					$scope.checkAdmin = function(){
				
					console.log("i got checking admin");
					if($scope.USER == $scope.pod_admin){
					
						console.log("you are the admin in this pod");
						angular.element(document.getElementById('approvals')).append($compile('<pod-approvals></pod-approvals>')($scope));
					}else{
						
						console.log("you are not admin in this pod");
						//angular.element(document.getElementById('approvals')).append("<h1>you are not subadmin /admin</h1>");
					}
				
			}
	
	
	
	$scope.loadDetail = function(){
			PodsDetails.getDetails($scope.POD_ID).then(function(data){
			$log.info("members pod approvals -> detail", data);
			
			$scope.pod_admin = data[0].pod_admin;
			
			var pod_subadmin = data[0].pod_subadmin;
			$log.info("pod_subadmin members pods approvals", $scope.pod_admin);
			
			try{
				
				for(var index = 0; index < pod_subadmin.length; index++){
			
					$scope.IDS.push(pod_subadmin[index].idkoolets_user);
				}
				
				console.log("IDS", $scope.IDS);
			
				
			
			}catch(err){
				
				$log.warn("No members", err);
			
			}
			
			var index = $.inArray($scope.USER, $scope.IDS);
				console.log(index)
				if(index < $scope.IDS.length && index != -1){
					
					console.log("you are pod-subadmin");
				angular.element(document.getElementById('approvals')).append($compile('<pod-approvals></pod-approvals>')($scope));
				
				}else{
					
					console.log("you are not pod-subadmin..checking admin");
					$scope.checkAdmin();
				}
		});
	}
		
			$scope.loadDetail();
					
		$scope.getMembersApproval =function(){
			
			
			//	Members.getMembers($scope.POD_ID).then(function(data){
			
			//$log.info("Members pod ->", data);
			
			//});
			
			
			PendingMembers.getPending($scope.POD_ID).then(function(data){
					$log.info("PENDING POD MEMBERS", data);
					
						$scope.pendings = data;
						$log.info("PENDINGS HERE ", $scope.pendings);
						$scope.membersPodApproval(data);
			});
			
		
		}
		
			$scope.membersPodApproval = function(_obj){
			
			$scope.members_list = _obj;
			$log.info("Members_list", $scope.members_list);
			try{
				
				for(var index= 0; index <= $scope.members_list.length; index++){
					
					PodSubAdminDetail.getUserDetails($scope.members_list[index].idkoolets_user).then(function(data){
					
						$log.info("members->>", data);
						$scope.MEMBER_APPROVAL.push(data.koolets_user[0]);
						
					});
				}
				
			}catch(err){
				
				
			}
			
			
		}
		
		$scope.getMembersApproval();
	
		//approve
		$scope.approveMember = function(_index){
			
			$log.info("APPROVE MEMBER CLICKED.", $scope.pendings[_index]);
			
			var formPending = $scope.pendings[_index];
			formPending.pod_status = true;
			
			PendingMembers.approvePending(formPending._id, formPending).then(function(data){
				$log.info("Success approved pending member.");
				$scope.MEMBER_APPROVAL.splice(_index, 1);
			});
		
		}
		
		//reject
		$scope.rejectMember = function(_index){
		
			var PENDING_MEMBER_ID = $scope.pendings[_index].idkoolets_user;
			
		
			$log.info("REJECT MEMBER CLICKED.");
			UnjoinPod.deleteMember($scope.POD_ID, PENDING_MEMBER_ID).then(function(data){
				console.log("you delete pending member.");
				
				$scope.MEMBER_APPROVAL.splice(_index, 1);
				
			});
		}
	

 });
 
 
 pods.directive('podApprovals', function(){
	return{
		template : '<div class="panel panel-default table-responsive">'+
					'<table class="table">'+
							'<thead>'+
							 ' <tr>'+
								'<td>#</td>'+
								'<td>Username</td>'+
								'<td>First Name</td>'+
								'<td>Last Name</td>'+
								'<td width="50">Approve</td>'+
								'<td width="50">Reject</td>'+
							 ' </tr>'+
							'</thead>'+
							'<tbody>'+
							'  <tr ng-repeat="member in MEMBER_APPROVAL">'+
								'<td><img ng-src="{{profile_url}}{{pendings[$index].idkoolets_user}}/64" class="thumb-sm avatar" style="margin-top: 8px"></td>'+
							'	<td><p style="margin-top:15px;">' +
							'{{member.username}}</p></td>'+
								'<td><p style="margin-top:15px;">{{member.fname}}</p></td>'+
							'	<td><p style="margin-top:15px;">{{member.lname}}</p></td>'+
							'	<td><md-button ng-click="approveMember($index)"><i class="fa fa-thumbs-o-up" ></i>&nbspApprove</md-button></td>'+
							'	<td><md-button ng-click="rejectMember($index)"><i class="fa fa-times" ></i>&nbspReject</md-button></td>'+
							'  </tr>'+
							'</tbody>'+
						 ' </table>'+
					'	</div>'
	}
 });

})()