(function(){

var pods = angular.module('DetailPod', []);

pods.controller('PodDetailController',function($log, $route,PodsDetails,$scope, PodBanner, $window, $mdDialog, JoinPod, $rootScope,PodSubAdminDetail, Profile,PodBanner, Members, $location, localStorageService, Invitee, $compile, PendingMembers){
	
	var self = this;
	$scope.admins = [];
	$scope.members = [];
	var  pod_id = $route.current.params._id;
	var membership_expiration; //declare
	
	$scope.user_id = localStorageService.get("koolets_user");
	
	$scope.detail_pod = {}; //declare
	
	$log.info("Route params pod id", $route.current.params._id);
	
	
	$scope.toggleJoin = false; //enable join button
	
	//banner
	$scope.urlBanner = PodBanner.getBannerUrl();
	
		//url
	$scope.profile_url = Profile.getUserProfilePicture();
	
	
	$scope.SUBADMIN_ID = [];
	
	//get invitees
		self.getInvitees = function(_pod_id){
		
			Invitee.getInvitee(_pod_id).then(function(data){
				
				$log.info("I got pod  invitees", data);
				
				if(angular.equals(data.length, undefined) || data.length <= 0){
					
					$scope.inviteesLength = 0;
					
				}else{
					
				$scope.inviteesLength = data.length;
				}
			});
	
			}
			
			
		self.getPendings = function(){
			
				PendingMembers.getPending(pod_id).then(function(data){
					$log.info("PENDING POD MEMBERS", data);
					
						$scope.pendingsCount = data.length;
						$log.info("PENDINGS HERE ", $scope.pendings);
						$scope.membersPodApproval(data);
			});
			
		}
		
		self.getPendings(); //call
			
				$scope.CHECKADMIN = function(){
				
					console.log("i got checking admin");
					if($scope.user_id  == $scope.pod_admin){
					
						console.log("you are the admin in this pod");
		angular.element(document.getElementById('approvalContainer')).append($compile('<member-approval></member-approval>')($scope));
		angular.element(document.getElementById('approvalContainer')).append($compile('<member-invitee></member-invitee>')($scope));
					}else{
						
						console.log("you are not admin in this pod");
						//angular.element(document.getElementById('approvalContainer')).append("<h1>you are not subadmin /admin</h1>");

					}
				
			}
			
			
	
		function SUB_ADMIN_APPROVAL(_obj){
				
			console.log("OBJ", _obj);	
			for(var index=0; index < _obj.length; index++){
				
				$scope.SUBADMIN_ID.push(_obj[index].idkoolets_user);
				
			}
			console.log($scope.SUBADMIN_ID);
			
				var index = $.inArray($scope.user_id, $scope.SUBADMIN_ID);
				console.log(index)
				if(index < $scope.SUBADMIN_ID.length && index != -1){
					
					console.log("you are pod-subadmin");
		angular.element(document.getElementById('approvalContainer')).append($compile('<member-approval></member-approval>')($scope));
		angular.element(document.getElementById('approvalContainer')).append($compile('<member-invitee></member-invitee>')($scope));	
				}else{
					
					console.log("you are not pod-subadmin..checking admin");
					$scope.CHECKADMIN();
				}
		}
	
	
	//fetch pod details using its id
	PodsDetails.getDetails(pod_id).then(function(data){
		$log.info("Details Pods", data[0]);
		
		$scope.detail_pod  = data[0];                                          //detail
		  
		 //membership expiration
		checkMembershipExpiration(data[0].pod_membership_expires);
		
		//membership status
		checkMembershipEnabled(data[0].pod_membership_enable);
		
		
		//pod visibility
		checkPodVisibility(data[0].pod_private);
		
		$scope.pod_admin = data[0].pod_admin;
		
		getSubAdminDetails(data[0].pod_subadmin);
		
		SUB_ADMIN_APPROVAL(data[0].pod_subadmin);
		
		$scope.pod_name = data[0].pod_title; // title
		$scope.podIdBanner = data[0]._id; // _id
		
		self.getInvitees(data[0]._id);
		$scope.POD_INVITEE_ID = data[0]._id;
		
		
		
	});
	
	
	$scope.pods_members = function(){
			$location.path('/pod-members/' + pod_id);
		}
	
	//show invitees
	$scope.goInvitees = function(){
		
		//$location.path('/pod-invitee/' + $scope.POD_INVITEE_ID);
		
		$location.path('/pod-invitee-setting/' + $scope.POD_INVITEE_ID);
	}

	
	//banner url
	$scope.pod_banner_url = PodBanner.getBannerUrl();
	
	
	//subAdmins
	function getSubAdminDetails(_obj){
		
		$scope.subAdmins = _obj; //-> contains user id's
		
		try{
		$scope.subAdminsLength = $scope.subAdmins.length; //length
		
			for(var index = 0; index <= $scope.subAdmins.length; index++){
				
				
				
				PodSubAdminDetail.getUserDetails($scope.subAdmins[index].idkoolets_user).then(function(data){
					$log.info("SubAdmin", data);
					$scope.admins.push(data.koolets_user[0]);
				});
			
			}
			
		
		}catch(err){
		
			$log.info("No subAdmins");
		}
			
	}
	
	
	$scope.usersID = [];
	//members
	
		$scope.membersPod = function(_obj){
			
			$scope.members_list = _obj;
			
			try{
				
				for(var index= 0; index <= $scope.members_list.length; index++){
					$scope.usersID.push($scope.members_list[index].idkoolets_user);
					PodSubAdminDetail.getUserDetails($scope.members_list[index].idkoolets_user).then(function(data){
					
						$log.info("members->>", data);
						$scope.members.push(data.koolets_user[0]);
						
					});
				}
				
				
				
			}catch(err){
				
				
			}
			
			$log.info("USERSID", $scope.usersID);
			if($.inArray($scope.user_id, $scope.usersID)){
			
				console.log("subadmin");
				//angular.element(document.getElementById('mobileMemberView')).append($compile('<h1>Not subadmin</h1>')($scope));
			
			}else{
				
				console.log("not subadmin");
			
			}
		}
	
	
	/**
	* checks pods visibility
	*/
	function checkPodVisibility(_status){
	
		if(angular.equals(_status, true)){
		
			$scope.podVisibility = 'Private';
		
		}else{
		
			$scope.podVisibility = 'Public';
		
		}
	}
	
	
	
	/**
	* pod membership enable filter
	* checks its boolean value if True display 'Enabled'
	* else display 'Disabled'
	*
	* Note: uses another scope to display text value
	*/
	function checkMembershipEnabled(_status){
		
		if(angular.equals(_status, true)){
			
			$scope.membershipStatus = 'Enabled';
		}else{
		
			$scope.membershipStatus = 'Disabled';
		
		}
	
	}
	
	
	
	
	
	/**pod membership expiration filter boolean
	* if pod_membership_expires is equal to true it will display 'Yes'
	* this means that membership  have an expiration
	*
	* if its false - it will display 'None' this means that
	* the pod admin did not enable membership expiration
	*
	* Note: it uses another scope to display the text value
	*/
	
	function checkMembershipExpiration(_status){
		
		if(angular.equals(_status, true)){
		
			$scope.membershipExpiration = 'Yes';
		
		}else{
		
			$scope.membershipExpiration = 'None';
		}
	
	} 
	
	//back button
	$scope.back = function(){
		$window.history.back();
	}
	
	//mobile view actions
	
	$scope.pod_mobile_view = 'info'; //initialize to default view 'General Info'
	
	$scope.switchMobile = function(_v){
	
		$scope.pod_mobile_view = _v;
		
		
	
	}
	
	//end mobile actions
	
	function joinedAlert(){
		$mdDialog.show(
			$mdDialog.alert()
					.title('Congrats!')
					.content('You have just joined a pod. Your pod is now added to your joined pod.')
					.ariaLabel('Join pod')
					.parent(angular.element(document.body))
					.ok('Ok')
		)
	};
	
	//join pod
	$scope.joinPod = function(_id){
		$log.info("Join pod", _id);
		
		JoinPod.podJoin(_id).then(function(data){
			
			joinedAlert() //alert when success joining the pod
			$scope.toggleJoin = true; //disable join button
			
		});
		
		
	}
	
	
	//member
	Members.getMembers(pod_id).then(function(data){
		$log.info("Pod Members here", data);
		
		
			$scope.podMembersLength = data.length;
			
			$scope.membersPod(data);
			
	
		
			//$scope.podMembersLength = 0;
		
		
	});

	
		$scope.goCreateEvent = function(){
			$rootScope.$emit("createEvent");
			$location.path("/event-create");
		}
		
		
		$scope.edit = function(_id){
			
			$location.path('/pod-settings/' + _id);
		}
		
			//member
			$scope.viewProfileDetail = function(_id){
			
			$location.path("/view-profile/" + _id);
		}
		
		//subadmin
		$scope.viewSubadminProfile = function(_id){
		
			$location.path('/view-profile/' + _id)
		
		}
	
});


//custom directive components for pod details

/**
* custom directive for pod location address
*/
pods.directive('podLocation', function($location, $log){
	return{
	 restrict : 'EACM',
	
	 template: '<div class="bg-danger wrapper hidden-vertical text-sm" ng-click="showPodMap(detail_pod.pod_location[0].latitude, detail_pod.pod_location[0].longitude,detail_pod.pod_location[0].address,detail_pod.pod_location[0].country)">' +            
			   '{{detail_pod.pod_location[0].address}}' +
			   '</div>',
	link: function(scope, elem, attrs){
		
		scope.showPodMap = function(_lat, _long, _a, _c){
		
			$location.path('/pod-map/' + _lat + '/' + _long + '/' + _a + '/' + _c);
		}
	 }
	}
 });

/**
* custom directive for pod banner
*/
pods.directive('podBanner',function(){
	return{
     restrict : 'EACM',
	 template : '<md-card>' + 
				'<img ng-src="{{pod_banner_url}}{{detail_pod._id}}" alt="pod_banner" />' +
				'</md-card>'
	}
});

/**
* pod general info directive
*/
pods.directive('podGeneralInfo', function(){
	return{
		restrict: 'EACM',
		template:'<div class="  col-lg-12">' +
                 '<div class="list-group bg-white">' +
                 '<header class="panel-heading bg-primary">General Info</header>' +
                      '<a class="list-group-item">' +
                  '<span class="pull-right ">{{detail_pod.pod_fees[0]}}</span>' +
               
              '   </i><strong>Fees</strong>' +
               ' </a>' +
               ' <a  class="list-group-item">' +
               '<span class="pull-right">{{podVisibility}}</span>' +
                 ' <strong>Visibility</strong>' +
                '</a> ' +
               ' <a  class="list-group-item">' +
                '<span class="pull-right">{{membershipStatus}}</span>' +
                  '<strong>Membership</strong>' +
                '</a> ' +
                 ' <a  class="list-group-item">' +
                '<span class="pull-right">{{membershipExpiration}}</span>' +
                  '<strong>Expiration</strong>' +
                '</a> ' +
              
                '<a  class="list-group-item">' +
                  '<span class="pull-right">{{detail_pod.pod_membership_duration}}</span>' +
                  '<strong>Duration</strong>' +
                '</a> ' +
                '<a  class="list-group-item">' +
                  '<span class="pull-right">{{detail_pod.pod_category}}</span>' +
                  ' <strong>Category</strong>' +
                '</a>' +
             '</div>' +
          '</div>'
	}
});


/**
* pod title
*/
pods.directive('podTitle', function(){
	return{
		restrict: 'EACM',
		template: '<div><h3>{{detail_pod.pod_title}}</h3></div>'
	}
});


pods.directive('podAdditionalInfo', function(){
	return{
		restrict : 'EACM',
		template : '<div class="wrapper"><header class="panel-heading bg-primary lter no-borders"><p>Additional info</p> </header>'+
					'<div class="list-group no-radius alt">'+
                   '     <a class="list-group-item" href="#" ng-repeat="additional in detail_pod.pod_extra">'+
                   
                    '      <i class="fa fa-comment icon-muted"></i> '+
                   '       {{additional.additionalInfo}}'+
                   '     </a>'+
					'    </div></div><br/><br/>'
	}
});


/**
* pod join
*/
pods.directive('podJoin', function(){

	return{
		restrict: 'EACM',
		template: '<div class="btn-group btn-group-justified m-b">' +
                  '<md-button class="md-raised md-primary" ng-disabled="toggleJoin" ng-click="joinPod(detail_pod._id)" flex>' +
                  '<span class="text">' +
                  '<i class="fa fa-plus"></i> Join Pod' +
                  '</span>' +
                  '</md-button>' + 
                  '</div>' 
	}

});


/**
* pod comments 
*/
pods.directive('podComment', function(){
	return{
		restrict: 'EACM',
		templateUrl: 'templates/pods/pod_comments.html'
	}
});

pods.directive('memberApproval', function(){

	return{
		restrict : 'EACM',
		template: '     <a class="list-group-item" href="#/pod-approval/{{detail_pod._id}}">'+
                         '<span class="badge bg-danger">{{pendingsCount}}</span>'+
                         ' <i class="fa fa-clock-o icon-muted"></i> '+
                       '  Approvals'+
                       ' </a>'
	}
});


pods.directive('memberInvitee', function(){
	return{
		restrict : 'EACM',
		template : '<a class="list-group-item" ng-click="goInvitees()">'+
                          '<span class="badge bg-success">{{inviteesLength}}</span>'+
                          '<i class="fa fa-eye icon-muted"></i> '+
                        ' Invitees'+
                       ' </a>'
	}
});
 
})()