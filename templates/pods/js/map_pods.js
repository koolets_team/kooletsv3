(function(){

	var pods = angular.module('PodMap', []);
	
	pods.controller('PodMapController', function($log, $route){
	
	var self = this;
	
	$log.info('Pod map controller is loaded')
	
	
	//coordinates of pod location
	self.mapCoordinates = {
		
		latitude : $route.current.params._lat,
		longitude: $route.current.params._long
		
	 }
	 
	 self.mapInformation = {
	 
		address: $route.current.params._address,
		country: $route.current.params._country
		
	 }
	
	
	});

	pods.directive('podBack', function($window){
		return{
			restrict : 'EACM',
			template: '<md-button class="md-fab md-fab-top-right" style="position:fixed; top: 50px; background-color:#337ab7">' +
					'	<md-tooltip>' +
					'	Back to pods' +
					'	</md-tooltip>' +
					'   <i class="fa fa-reply-all"></i>' +
					'   </md-button>',
			link: function(scope, elem, attrs){
				
				elem.bind('click', function(){
					
					$window.history.back();
				});
				
			}
		}
	});
	
})()