(function(){

	var pods = angular.module('PodRequired', ['ngRoute']);
	
	pods.controller('PodRequiredFieldsController', function($rootScope, $log,  $scope, $route, $timeout, PodsDetails, JoinPod, $mdDialog, $location,  Profile){
		
			var DIRECT_STATUS = true;
			var PENDING_STATUS = false;
		
			var pod_id = $route.current.params._id;
			$log.info("Required", $route.current.params._id);
			
			//url
		$scope.profile_url = Profile.getUserProfilePicture();
			
			
				//fetch pod details using its id
	PodsDetails.getDetails(pod_id).then(function(data){
		
		$scope.pod_title = data[0].pod_title;
		
		$scope.extraFields =data[0].pod_extra;
		$log.info("extra",data[0].pod_extra);
		$scope.approval = data[0].pod_approval;
	});
	
	
	$scope.joinPod = function(ev){
		if($scope.approval){
		
		
				JoinPod.podJoin(pod_id, $scope.extraFields, PENDING_STATUS).then(function(data){
					
					$log.info("successfully join");
					$scope.joinedAlert(ev);
				});
		
		}else{
		
		
				JoinPod.podJoin(pod_id, $scope.extraFields, DIRECT_STATUS).then(function(data){
					
					$log.info("successfully join");
					$scope.joinedAlert(ev);
				});
		}
		
		
	}
	
	
			$scope.join = function(_approval, ev){
			
				$log.info("Filled required fields", $scope.extraFields);
				JoinPod.podJoin(pod_id, $scope.extraFields, _approval).then(function(data){
					
					$log.info("successfully join");
					$scope.joinedAlert(ev);
				});
			
			
			}
	
				$scope.joinedAlert = function(ev){
		$mdDialog.show(
			$mdDialog.alert()
					.title('Congrats!')
					.content('You have just joined a pod. Your pod is now added to your joined pod.')
					.ariaLabel('Join pod')
					.parent(angular.element(document.body))
					.ok('Ok')
					.targetEvent(ev)
		).then(function(){
				$location.path('/joined-pods');	
		});
	};
	
	
		$scope.testJoin = function(){
			$log.info("I got testJoin");
		}
	
	});
	
})()