(function(){
	var pods = angular.module('PodSubAdmins', []);
	
	pods.controller('PodSubadminController', function(PodsDetails,PodSubAdminDetail, $route, $log,$scope, Profile,PodBanner,  $location){
	
		
	var self = this;
	var pod_id = $route.current.params._id;
	$scope.admins = [];
			//fetch pod details using its id
	PodsDetails.getDetails(pod_id).then(function(data){
		
		$scope.getSubAdminDetails(data[0].pod_subadmin);
		$scope.pod_name = data[0].pod_title; //title
		$scope.podIdBanner = data[0]._id;
	});
	
	//url profile
	$scope.profile_url = Profile.getUserProfilePicture();
	
	//banner
	$scope.urlBanner = PodBanner.getBannerUrl();
	
		//subAdmins
	$scope.getSubAdminDetails = function(_obj){
		
		$scope.subAdmins = _obj; //-> contains user id's
		$log.info("subADmins", $scope.subAdmins);
		try{
		
			for(var index = 0; index <= $scope.subAdmins.length; index++){
			
				PodSubAdminDetail.getUserDetails($scope.subAdmins[index].idkoolets_user).then(function(data){
				
//					$log.info("Ids",data.koolets_user[0].$$hashKey);
					var tt = angular.toJson(data.koolets_user[0]);
					
					$scope.admins.push(data.koolets_user[0]);
				});
			
			}
		
		}catch(err){
		
			$log.info("No subAdmins");
		}
		
	}
		
		
		$scope.viewSubadminProfile = function(_id){
		
			$location.path('/view-profile/' + _id)
		
		}
	
	});
	
	pods.directive('podSubadminList', function(){
		return{
			restrict : 'EACM',
			templateUrl: "templates/pods/partials/pod_subadmin.html"
             
		}
	});
	
})()

