(function(){

var pods = angular.module('PodInvites', []);

pods.controller('PodInviteesController', function($log, $route, Invitee,PodSubAdminDetail, Profile, localStorageService, $compile, $scope, PodsDetails, $mdDialog){
	
	var self = this;
	$scope.invited = [];
	var pod_id = $route.current.params.pod_id;
	$log.info("PodInviteesController is loaded.", $route.current.params.pod_id);
	
	$scope.user_id = localStorageService.get("koolets_user");
	
	
	
	
	//url
		$scope.profile_url = Profile.getUserProfilePicture();
		
			PodsDetails.getDetails(pod_id).then(function(data){
			$log.info("members pod subs -> detail", data);
			
			var pod_subadmin = data[0].pod_subadmin;
			$log.info("pod_subadmin members pods", pod_subadmin);
			
			
	
		});
		
		
				
				$scope.CHECKADMIN = function(){
				
					console.log("i got checking admin");
					if($scope.user_id  == $scope.pod_admin){
							self.getInvitees(pod_id);
						console.log("you are the admin in this pod");
			angular.element(document.getElementById('inviteeCon')).append($compile('<pod-invitee-sub></pod-invitee-sub>')($scope));
		//	angular.element(document.getElementById('inviteeCon">')).append($compile("<h1>asdfsdasd</h1>")($scope));
					}else{
						
						console.log("you are not admin in this pod");
						//angular.element(document.getElementById('inviteeCon">')).append("<h1>you are not subadmin /admin</h1>");
			//	angular.element(document.getElementById('inviteeCon">')).append($compile("<h1>asdfsdasd</h1>")($scope));
					}
				
			}
		
		
		//pod detail
			PodsDetails.getDetails(pod_id).then(function(data){
		$log.info("Details Pods subsSSS", data[0].pod_subadmin);
		
		$scope.pod_admin = data[0].pod_admin;
	
		$scope.subs = data[0].pod_subadmin;
		$scope.SUBADMIN_ID = [];
		for(var i = 0; i < $scope.subs.length; i++){
			
			$scope.SUBADMIN_ID.push($scope.subs[i].idkoolets_user);
		}
	
				var index = $.inArray($scope.user_id, $scope.SUBADMIN_ID);
				console.log(index)
				if(index < $scope.SUBADMIN_ID.length && index != -1){
				
					self.getInvitees(pod_id);
					
					console.log("you are pod-subadmin");
		angular.element(document.getElementById('inviteeCon')).append($compile('<pod-invitee-sub></pod-invitee-sub>')($scope));
				
				}else{
					
					console.log("you are not pod-subadmin..checking admin");
				$scope.CHECKADMIN();
				}
		
	});
		
				
			
		
		
			//get invitees
		self.getInvitees = function(pod_id){
		
			Invitee.getInvitee(pod_id).then(function(data){
			
				$scope.inviteesID = data;
				
				$log.info("I got pod  invitees", data);
				try{
				
				for(var index= 0; index < data.length; index++){
					
					$scope.SUBADMIN_ID = data[index].idkoolets_user;
					
					PodSubAdminDetail.getUserDetails(data[index].idkoolets_user).then(function(data){
					
						$log.info("invitees->>", data);
						$scope.invited.push(data.koolets_user[0]);
						
					});
				}
				
				
		
				
				
			}catch(err){
				
				
				}
				
			});
	
			}
	

	//remove invitee
	$scope.removeInvitee = function(_id, _index){
		
		//$log.info("remove invitee clicked", _id);
		//Invitee.removeInvitee(pod_id, _id).then(function(data){
			
		//	$log.info("You remove invitee.");
			
			
		//});
		//$scope.inviteesID.splice(_index, 1);
		//$scope.invited.splice(_index ,1);
		
		
		
		var confirmLeave = $mdDialog.confirm()
										 .title('Remove invitee')
					                     .content('Are you sure you want to remove invitee?')
					                     .ok('Kick')
					                     .cancel('Cancel')
					                     .ariaLabel('leave invitee')
					 
		$mdDialog.show(confirmLeave).then(function(){
			
			$log.info("I got leave");
			
				$log.info("remove invitee clicked", _id);
				Invitee.removeInvitee(pod_id, _id).then(function(data){
			
					$log.info("You remove invitee.");
			});
				
				$scope.inviteesID.splice(_index, 1);
				
				$scope.invited.splice(_index ,1);
			
		}, function(){
		
			$log.info("I canceled");
		
		});
		
		
		
	}
	
});


pods.directive('podInviteeSub', function(){
		return{
			restrict : 'EACM',
			template : '<ul class="list-group no-radius m-b-none m-t-n-xxs list-group-alt list-group-lg">' +
               ' <li class="list-group-item" ng-repeat="member in invited | filter:filterMember" ng-click="viewProfileDetail(inviteesID[$index].idkoolets_user)">' +
                  '   <a href="#" class="thumb-xs pull-left m-r-sm">' +
            '<img ng-src="{{profile_url}}{{inviteesID[$index].idkoolets_user}}/64" class="img-circle">' +
               '   </a>' +
               '   <a class="clear">' +
			   '<small class="pull-right"><i class="fa fa-times" ng-click="removeInvitee(inviteesID[$index].idkoolets_user, $index)"></i></small>' +
               '     <strong>{{member.fname}}&nbsp{{member.mname}}&nbsp{{member.lname}}</strong>'+
               '     <span></span>' +
               '   </a>' +
               ' </li>' +
			   '</ul>'
			
		}
	});
	
	
	
		pods.directive('podInviteeList', function(){
		return{
			restrict : 'EACM',
			template : '<ul class="list-group no-radius m-b-none m-t-n-xxs list-group-alt list-group-lg">' +
               ' <li class="list-group-item" ng-repeat="member in invited | filter:filterMember" ng-click="viewProfileDetail(inviteesID[$index].idkoolets_user)">' +
               '   <a href="#" class="thumb-xs pull-left m-r-sm">' +
            '<img ng-src="{{profile_url}}{{inviteesID[$index].idkoolets_user}}/64" class="img-circle">' +
               '   </a>' +
               '   <a class="clear">' +
			   '<small class="pull-right"><i class="fa fa-circle text-info text-xs"></i></small>' +
               '     <strong>{{member.fname}}&nbsp{{member.mname}}&nbsp{{member.lname}}</strong>'+
               '     <span></span>' +
               '   </a>' +
               ' </li>' +
			   '</ul>'
			
		}
	});

})()