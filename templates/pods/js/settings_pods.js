(function(){

	var pods  = angular.module('PodSettings', []);
	
	pods.controller('PodSettingsController', function($log,wildCardSearch_subAdmin,podSubminService ,$scope, $route,PodsDetails,NavigatorGeolocation, $scope, GeoCoder,PodBanner, $mdDialog, Upload,PodBannerUpload, baseUrl, portPods, UpdatePod, $window,$location){
	
		var reverse  = new google.maps.Geocoder();
		var latlng;
	
		var self = this;
		
		self.urlBanner = PodBanner.getBannerUrl();
		
		$scope.pod_id = $route.current.params._id;
		self.Form = {};
		
		
			$scope.$watch('pod_location', function(newVal, oldVal){

				if(angular.equals(newVal, "") || angular.equals(newVal, undefined)){
					
					
					self.checkboxToggle = false;
					
				}else{
					
					self.checkboxToggle = true;
					
				   GeoCoder.geocode({address : newVal}).then(function(data){
					$scope.searchLocations = data;
					console.log(data);
				  });
				}
			});
	
			$scope.locationPodSearch = function(location){

				var splitAddress = location.formatted_address.split(',');

			$scope.locationSelected ={
				"address" : location.formatted_address,
				"place_id" : location.place_id,
				"country" : splitAddress[splitAddress.length - 1],
				"latitude" : location.geometry.location.G,
				"longitude" : location.geometry.location.K
			};
			
			self.Form.pod_location = $scope.locationSelected;
			return location.formatted_address;
		}

		
			self.checkInStatus = function(){
			
				if(angular.equals(self.checkInState, true)){
				
					NavigatorGeolocation.getCurrentPosition().then(function(position){
				
						latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				
						reverse.geocode({'location' : latlng},function(result, status){
				
							if(angular.equals(status, google.maps.GeocoderStatus.OK)){
						
								var splitAddress = result[0].formatted_address.split(',');

							$scope.locationSelected ={
									"address" : result[0].formatted_address,
									"place_id" : result[0].place_id,
									"country" : splitAddress[splitAddress.length - 1],
									"latitude" : result[0].geometry.location.G,
									"longitude" : result[0].geometry.location.K
								};
							
									self.Form.pod_location = $scope.locationSelected;
									console.log("adasdfkljashdklf", $scope.locationSelected)
							}	
				
						});
				
					});
				
					self.searchInputPlace = true; //disable search place
				
				}else{
				
					self.searchInputPlace = false; //disable search place
				}
			
			}
			
			
		
		$log.info("PodSettings controller is loaded.");
		
		PodsDetails.getDetails($scope.pod_id).then(function(data){
		$log.info("Edit Pods", data[0]);
		
			$log.info("Setting pods", data);
			
			self.Form.pod_title = data[0].pod_title;
			self.Form.pod_category = data[0].pod_category;
			self.Form.pod_summary = data[0].pod_summary;
			self.Form.pod_fees = parseInt(data[0].pod_fees[0]);
			self.Form._id = data[0]._id;
			self.Form.pod_membership_enable  = data[0].pod_membership_enable;
			$scope.pod_location = data[0].pod_location[0].address;
			self.Form.pod_membership_duration = data[0].pod_membership_duration;
			self.Form.pod_membership_expires = data[0].pod_membership_expires;
			self.Form.pod_private = data[0].pod_private;
			self.Form.pod_approval = data[0].pod_approval;
			membershipToggle();
			$scope._subAdmins = data[0].pod_subadmin;
			
			self.Form.pod_extra = data[0].pod_extra;
			
		});
		
		self.addField = function(){
			
			self.Form.pod_extra.push({"FieldName": self.fieldName, "FieldValue" : null});
		
		}
		
		
		self.removeField = function(_index){
			
			self.Form.pod_extra.splice(_index, 1);
		
		}
		
		
		//-- Upload photo banner
		var podFile_Banner;
				$scope.podBannerUpload = function(podId){
   
			Upload.upload({
				url:baseUrl + portPods + PodBannerUpload + $scope.pod_id,
				data :$scope.pod_id,
				file : podFile_Banner
				}).progress(function(evt){
				$scope.progressUpload = parseInt(100.0 * evt.loaded / evt.total);
			}).success(function(data, status, headers, config){
				console.log(data);
				console.log("pod banner success");
				$mdDialog.hide();
			
			}).error(function(data, status, headers, config){
				console.log("pod banner error");

			})
 		
		}
			$scope.showDefaultPod = true;
			$scope.previewPodPic = false;
				$scope.$watch('podBanner', function(){
					$scope.podBannerChange($scope.podBanner);
				});

		$scope.podBannerChange = function(files){
			if(files && files.length){
				podFile_Banner = files[0];
				$scope.previewPodPic = true;
				$scope.showDefaultPod = false;
				$scope.uploadShow = true;
			}
		}
		
		
		$scope.uploadNewPhoto = function(ev){
			$mdDialog.show({
				controller : 'PodSettingsController',
				templateUrl : 'templates/partials/pic_upload.html',
				parent : angular.element(document.body)
			});
		}
		//--End upload photo banner --//
		
		//edit and save changes
		self.edit = function(_id){
		
			membershipEnableChanged()
		
			console.log("id edit", _id);
			UpdatePod.editPod(_id, self.Form).then(function(data){
				$log.info("Congrats!. you can now update pod");
				$mdDialog.show(
					$mdDialog.alert()
							.ariaLabel('pod edit save')
							.title('Pod Saved.')
							.content('Changes to pod has been saved.')
							.parent(angular.element(document.body))
							.ok('Ok')
				)
				
			});
		}
		
		
			//--membership enable
			function membershipToggle(){
					
					if(angular.equals(self.Form.pod_membership_enable, true)){
					
						
						$scope.toggleMembership = true;
					
					}else{
						$scope.toggleMembership = false;
					}
				
				}
		
			$scope.membershipEnableStatus = function(){
					
					if(angular.equals(self.Form.pod_membership_enable, true)){
					
						
						$scope.toggleMembership = true;
					
					}else{
						$scope.toggleMembership = false;
					}
				
				}
				
			// membership enable button is unchecked set membership duration and expires to its default value
			function membershipEnableChanged(){
				if(angular.equals(self.Form.pod_membership_enable, true)){
				}else{
				
						self.Form.pod_membership_duration = 0;
						self.Form.pod_membership_expires = false;
				
				}
			}
				
			//--End membership enable
		
		//typeahead search
	$scope.subAdminSearch = function(_sub_admin){
	console.log($scope.podAdminAdd);
	$scope.$watch('podAdminAdd', function(){
		$scope.podAdminAdd = $scope.podAdminAdd;
	});

	if(angular.equals($scope.podAdminAdd, "")){

	}else{
		wildCardSearch_subAdmin.searchUser_wildCard($scope.podAdminAdd).then(function(data){
		console.log(data.koolets_user);
		$scope.resultSearched= data.koolets_user;
		console.log($scope.resultSearched, "<><>");
		});
	  }
	}	
	
	$scope.subAdminPod = function(pods_subadmin){
		$scope.selectedPodSubAdmin = pods_subadmin;
		return pods_subadmin.lname + ',' + pods_subadmin.fname + ',' + pods_subadmin.mname;
	}
	
	$scope.addAdmin=function(){
		podSubminService.getSubmins($scope.selectedPodSubAdmin);
		$scope._subAdmins = podSubminService.setSubmins();
		
		$scope.podAdminAdd = "";
		console.log($scope._subAdmins.length);
		
		
		}
		
		
	
	$scope.removeSubAdmin= function(index){

		$scope._subAdmins.splice(index, 1);
		console.log(index);
		console.log($scope._subAdmins);
		$scope.$watch('_subAdmins', function(){
				$scope._subAdmins = $scope._subAdmins;
		});
		
	}

	//typeahead end
	
	//subadmins
	self.subadmins = function(){
		$log.info("go subadmins");
		$location.path('/pod-subadmins-setting/' + $scope.pod_id);
	}
	
	//member
	self.members = function(){
		$log.info("go members");
		$location.path('/pod-members-setting/' + $scope.pod_id);
	}
	
	
	$scope.closeUpload = function(){
			$mdDialog.hide();
		}
		
			//invitees
	self.invitees = function(){
		$log.info("go invitee");
		$location.path('/pod-invitee-setting/' + $scope.pod_id);
	}
	
	
	
		
});
	

	

})()