(function(){
	
	var pods = angular.module('SettingMembers', []);
	
	pods.controller('SettingMembersController', function(Members, $log, $route, Profile, PodSubAdminDetail, $scope){
	
		
		var self = this;
		var pod_id = $route.current.params._id
	
		self.members_list = [];
	
		//url
		$scope.profile_url = Profile.getUserProfilePicture();
	
		self.membersPodLoad = function(){
			
				Members.getMembers(pod_id).then(function(data){
			
					self.members = data;
					$log.info("Members settings", data);
					$log.info("Members settings length", self.members.length);
					self.getDetails(data);
				});
			
		}
		
		self.membersPodLoad() //call
	
	
		self.getDetails = function(_obj){
			
			for(var index= 0; index < _obj.length; index++){
					
					PodSubAdminDetail.getUserDetails(_obj[index].idkoolets_user).then(function(data){
					
						$log.info("members setttings->>", data);
						self.members_list.push(data.koolets_user[0]);
						
					});
				}
			
		}
		
		$scope.removeMember = function(_id, _index){
			
			self.members_list.splice(_index, 1);
			
		}
		
	
	});
	
})()