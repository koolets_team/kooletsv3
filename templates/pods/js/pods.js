/*
*PodsService module is used here
*/
(function(){
	var pods = angular.module('Pods', ['PodsService', ]);
	
	pods.controller('PodsController', function(PublicPods, PodBanner,$log, $location, Profile, $mdDialog,JoinPod, $rootScope, $location, $timeout, $mdDialog, $scope){
		
		var self = this;
		
		//reload profile url
			$rootScope.$broadcast("reloadUrl");
		
		console.log("Pods controller is loaded");
		
		function fetchPublicPods(){
		
			PublicPods.getPublicPod().then(function(data){
				$log.info("data", data);
				self.pods = data;
			});
		
		
		}
		
		
		fetchPublicPods() //call
		
		self.profile_url = Profile.getUserProfilePicture();
		
		
		$log.info(PodBanner.getBannerUrl());
		
		self.urlBanner = PodBanner.getBannerUrl();
		
		//route to joined-pods page
		self.joinedPods = function(){
			$location.path('/joined-pods');
		};
		
		
		//route to my-pods page - user created pods
		self.myPods = function(){
		
			$location.path('/my-pods');
		};
		
		//pod id
		self.detail = function(_id){
			$log.info(_id);
			$location.path('/detail-pod/' + _id);
		}
		
		$scope.test = "test scope";
		self.fields= [];
	
		//join pod
		self.userJoinPod = function(_id, _pod_approval, _pod_extra ,ev){
			$log.info("User joined pod.", _id);
			self._ID = _id;
			$log.info("User _pod_approval.", _pod_approval);
			
			self._POD_APPROVAL = _pod_approval;
			
			self.DIRECT_STATUS = true;
			self.PENDING_STATUS = false;
			
			$log.info("User _pod_extra.",  _pod_extra);
			
			
			
				if(!_pod_extra.length <= 0 ){
				
					$log.info("need to fill up");
					$location.path('/pod-join-required/' + _id);
					
				}else{
				
				
					if(self._POD_APPROVAL){
					
					
								JoinPod.podJoin(self._ID, null , self.PENDING_STATUS).then(function(data){
									$log.info("Success direct user join");
									self.joinedAlert();
							});
					
					}else{
					
								JoinPod.podJoin(self._ID, null , self.DIRECT_STATUS).then(function(data){
										$log.info("Success direct user join");
										self.joinedAlert();
								});
									
					}
						
					
						
				}
		}
		
	
		
		
			self.joinedAlert = function(ev){
		$mdDialog.show(
			$mdDialog.alert()
					.title('Congrats!')
					.content('You have just joined a pod. Your pod is now added to your joined pod.')
					.ariaLabel('Join pod')
					.parent(angular.element(document.body))
					.ok('Ok')
					.targetEvent(ev)
		).then(function(){
				fetchPublicPods();
		});
	};
		

		
		
	});
	

})()