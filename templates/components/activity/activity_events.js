/**
 * 
 */
(function() {
	var activityEvents = angular.module('ActivityEvents', []);
    
       activityEvents.controller('ActivityEventsController',ActivityEventsController);
       activityEvents.factory('ActivityEventsService', ActivityEventsService);
       
       ActivityEventsController.$inject= ['$routeParams', '$log','ActivityEventsService']; 
       function ActivityEventsController($routeParams, $log,ActivityEventsService){
    	   var vm = this;
    	   vm.recentListActivities = [];
    	   
    	   recentActivity(_koolets_id);
    	   
    	   function recentActivity(_koolets_id){
    		   return getRecentActivityList(_koolets_id).then(function(){
    			   $log.info('Activated Avengers View');
    		   });
    	   }
    	   
    	   function getRecentActivityList(_koolets_id) {
    	        return ActivityEventsService.getRecentActivity(_koolets_id)
    	            .then(function(data) {
    	                vm.recentListActivities = data;
    	                return vm.recentListActivities;
    	            });
    	    }

       }
       
       ActivityEventsService.$inject = ['$http','baseUrl','portEvent','EventRecent'];
       function ActivityEventsService($http,baseUrl,portEvent,EventRecent){
    	   var service = {
    		        getRecentActivity: getRecentActivity //,
    		        //getUpcomingActivity: getUpcomingActivity
    		    };
    	   
    	   return service;
    	   
    	   
    	   function getRecentActivity(_koolets_id) {
    	        return $http.get(baseUrl+portEvent+EventRecent+_koolets_id)
    	            .then(getComplete,getFailed);

    	        function getComplete(response) {
    	            return response.data.results;
    	        }

    	        function getFailed(error) {
    	            //logger.error('XHR Failed for getAvengers.' + error.data);
    	            return error;
    	        }
    	    }
    	   
    	  
    	   
    	   function getUpcomingActivity(_koolets_id){
    		   
    	   }
    	   
       }
       
       
});