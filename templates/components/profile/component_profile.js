(function() {
	var componentProfile = angular.module('ComponentProfile', []);

	componentProfile.controller('ComponentProfileController', function($scope,
			$routeParams, $log, ComponentProfileService) {

		var id_user = $routeParams._id;
		var vm = this;

		vm.about = ComponentProfileService.getUserDetails(id_user).then(
				function(_data) {
					vm.koolets_user = _data;
				}, function(data) {
					vm.koolets_user = null;
				});

        vm.photo_url = ComponentProfileService.getUrl(id_user);
        vm.qr_url = ComponentProfileService.getUrlQR(id_user);

        vm.count = ComponentProfileService.getPodCountCreated(id_user).then(
            function(_data){


                vm.podCount = _data.data;
                //console.log("das1"+( vm.podCount.size));
            },
            function (error) {
                return error;
            });



    });

    componentProfile.directive('componentPodCount',function(){
         return {
             templateUrl: 'templates/components/profile/profile_counts.html',
             controller:'ComponentProfileController',
             controllerAs:'ComponentProfile'

         }
    })

    componentProfile.directive('componentShortProfileView',function(){
       return {
           templateUrl:'templates/components/profile/profile_short.html',
           controller:'ComponentProfileController',
           controllerAs:'ComponentProfile'
       }
    });

	componentProfile.directive('componentProfileView', function() {
		return {
			templateUrl : 'templates/components/profile/profile_view.html',
			controller : 'ComponentProfileController',
			controllerAs : 'ComponentProfile'

		}
	});

	componentProfile.directive('componentContactView', function() {
		return {
			templateUrl : 'templates/components/profile/contact_view.html',
			controller : 'ComponentProfileController',
			controllerAs : 'ComponentProfile'
		}
	});




	componentProfile.factory('ComponentProfileService', function($http, $log,
			baseUrl, portUser, userDetails, PodCreatedCount,portPods,userQR) {
		return {
			getUserDetails : function(_koolets_user_id) {
				return $http.get(
						baseUrl + portUser + userDetails + _koolets_user_id)
						.then(function(_details) {
							return _details.data.koolets_user[0];
						});
			},
            getUrl : function(_koolets_user_id){
                var url = baseUrl + portUser +"getUserProfilePic/"+ _koolets_user_id;
                return url;
            },
            getPodCountCreated : function (_koolets_user_id){
                return $http.get(baseUrl + portPods + PodCreatedCount + _koolets_user_id)
                    .then(function (_details){

                       return _details;
                    });
            },


                    getUrlQR : function(_koolets_user_id){
                        var qr = baseUrl  + portUser + userQR+_koolets_user_id;

                        return qr;
                    }



		}
	});

})()