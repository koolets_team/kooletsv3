(function(){
	
	var koolets = angular.module('KooletsDirectives',[]);
	
	koolets.directive('backButtonNormal',function($window){
		return{
			restrict : 'EACM',
			template: '<button  class="btn btn-success pull-right" hide-sm><i class="fa fa-angle-left text-white"></i></button>',
			link : function(scope, elem, attrs){
				
				elem.bind('click', function(){
					$window.history.back();
				});
			}
			
		}
	});
	
	
	koolets.directive('accountDropmenu', function(){
		return{
			restrict : 'EACM',
			template : '<ul class="nav navbar-nav navbar-right" hide-sm >'+
			'<li class="dropdown"  md-ink-ripple>'+
			'  <a id="accountmenu" data-target="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'+
            '   <span><i class="fa fa-bell-o"></i></span>'+
           '  </a>'+
			'  </li>'+
            '<li class="dropdown"  md-ink-ripple>'+
            '  <a id="accountmenu" data-target="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'+
             '   <span><i class="fa fa-user"></i>&nbsp&nbspAccount</span>'+
            '    <span class="caret"></span>'+
            '  </a>'+
            '  <ul class="dropdown-menu" aria-labelledby="accountmenu">'+
            '    <li><a href="#/profile"><i class="fa fa-user"></i>&nbspView Profile</a></li>'+
            '   <li><a href="#/profile-settings"><i class="fa fa-gear"></i>&nbspEdit Account</a></li>'+
             '   <li role="separator" class="divider"></li>'+
            '    <li><a ng-click="mainKoolets.logout()"><i class="fa fa-sign-out"></i>&nbspLogout</a></li>'+
            '  </ul>'+
          '  </li>'+
          ' </ul>',
		 controller : 'MainController',
		 controllerAs : 'mainKoolets',
		 bindToController : true
		}
	});
	
}())