/*application router*/
(function(){

var route = angular.module('appRouter', ['ngRoute']);

	route.config(function($routeProvider){
		$routeProvider.when('/signin',{
			templateUrl: 'templates/login/login.html',
			controller : 'LoginController',
			controllerAs: 'appLogin'
		}).when('/main',{
			
			template: '<h1>Koolets</h1>'
		
		}).when('/pods',{
			
			templateUrl: 'templates/pods/pods.html',
			controller: 'PodsController',
			controllerAs: 'appPods'
			
		}).when('/joined-pods',{
			
			templateUrl: 'templates/pods/joined_pods.html',
			controller: 'JoinedPodController',
			controllerAs : 'podJoined'
			
		}).when('/my-pods',{
		
			templateUrl: 'templates/pods/created_pods.html',
			controller: 'CreatedPodController',
			controllerAs : 'createdPod'
			
		}).when('/create-new',{
		
			template : '<create-new-pod></create-new-pod>',
			controller: 'CreateNewPodController',
			controllerAs : 'createPod'
		
		}).when('/detail-pod/:_id', {
			
			templateUrl : 'templates/pods/detail_pods.html',
			controller: 'PodDetailController',
			
			
			
		}).when('/pod-map/:_lat/:_long/:_address/:_country',{
		
			templateUrl : 'templates/pods/pod_map.html',
			controller: 'PodMapController',
			controllerAs : 'mapPod'
			
		}).when('/pod-settings/:_id', {
		
			templateUrl : 'templates/pods/pod_settings.html',
			controller : 'PodSettingsController',
			controllerAs : 'settingPod'
			
		}).when('/pod-subadmin/:_id',{
		
			templateUrl : 'templates/pods/pod_subadmin.html',
			controller : 'PodSubadminController',
			controllerAs : 'subadminPod'
			
		}).when('/pod-members/:_id',{
		
			templateUrl : 'templates/pods/pod_members.html',
			controller : 'PodMemberController',
			controllerAs : 'podmemberPod'
			
			
		}).when('/pod-subadmins-setting/:_id',{
		
			templateUrl : 'templates/pods/subadmin.html',
			controller : 'SettingSubadminsController',
			controllerAs : 'settingSubadmin'
		
		}).when('/pod-members-setting/:_id',{
		
			templateUrl : 'templates/pods/members.html',
			controller : 'SettingMembersController',
			controllerAs : 'settingMember'
			
		}).when('/pod-invitee-setting/:pod_id', {
			
			templateUrl : 'templates/pods/invitee.html',
			controller : 'InviteeSettingController',
		
			
		}).when('/pod-invitee/:pod_id',{
			
			templateUrl: 'templates/pods/pod_invitee.html',
			controller : 'PodInviteesController',
			
		
		}).when('/pod-join-required/:_id',{
		
			templateUrl : 'templates/pods/pod_required_field.html',
			controller : 'PodRequiredFieldsController'
			
		}).when('/pod-approval/:_id',{
			
			templateUrl : 'templates/pods/pod_approvals.html',
			controller : 'PodApprovalsController'
		
		}).when('/events',{
		
			templateUrl : 'templates/events/events.html',
			controller : 'EventsController',
			controllerAs : 'appEvents'
		
		}).when('/events-joined',{
			
			templateUrl : 'templates/events/events_joined.html',
			controller : 'EventsJoinedController',
			controllerAs : 'eventsJoined'
		
		}).when('/event-create',{
		
			
			templateUrl : 'templates/events/event_create.html',
			controller: 'EventCreateController',
			controllerAs : 'newEvent'
		
		}).when('/event-details/:_id', {
		
			templateUrl : 'templates/events/event_details.html',
			controller : 'EventDetailController',
			controllerAs : 'detailEvent'
		
		
		}).when('/event-settings/:_id',{
		
			templateUrl: 'templates/events/edit_event.html',
			controller: 'EventSettingController',
			controllerAs : 'settingEvent'
			
		}).when('/event-members/:_id',{
		
			templateUrl : 'templates/events/event_members.html',
			controller : 'EventMembersController',
			controllerAs : 'eventMember'
		
		}).when('/event-private-invitee/:_eid', {
		
			
			templateUrl : 'templates/events/event_invitee.html',
			controller : 'EventPrivateInviteeController',
			controllerAs : 'eventInvitee'
		
		}).when('/event-invitee-setting/:_eid', {
			
			templateUrl : 'templates/events/event_invitee_setting.html',
			controller: 'EventInviteeSettingController'
		
		}).when('/discovery',{
		
			templateUrl : 'templates/discovery/discovery.html',
			controller : 'DiscoveryController',
			controllerAs : 'discovery'
			
		}).when('/profile-settings',{
			
			templateUrl : 'templates/profile/profile.html',
			controller : 'ProfileSettingController',
			controllerAs : 'profileSetting'
			
		}).when('/profile', {
		
			templateUrl : 'templates/profile/profile_detail.html',
			controller : 'ProfileDetailController',
			controllerAs : 'profileDetail'
		
		}).when('/view-profile/:_id',{
		
			templateUrl: 'templates/profile/profile_view.html',
			controller : 'ProfileViewController',
			controllerAs : 'profileView'
		
		}).when('/register',{
		
			templateUrl : 'templates/register/registration.html',
			controller: 'RegistrationController',
			controllerAs : 'appRegister',
			
		}).when('/messages',  {
			
			templateUrl : 'templates/messages/messages.html'
			
		}).otherwise('/profile');
	});

})();