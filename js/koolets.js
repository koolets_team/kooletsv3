(function(){

var koolets = angular.module('Koolets', []);

koolets.controller('MainController',  function(LoggedIn, $scope, $log, $location, localStorageService, $mdSidenav, $rootScope, $scope,DiscoveryEvents, ProfilePhoto, $scope, $mdBottomSheet, ClearLocalStorage){
	
		DiscoveryEvents.getEvents();
		DiscoveryEvents.setUserLocation();

	
	LoggedIn.isUserLoggedIn();
	
	
	
	var self = this;
	
	self.sideNavState = localStorageService.get("MenuState");
	
	
	$scope.$on("ShowNav", function(){
		console.log("Show Nav here");
		self.sideNavState = localStorageService.get("MenuState")
		
	});
	
	
	self.discovery = function(){
		$location.path('/discovery');
	}
	
		self.pods = function(){
		$location.path('/pods');
	}
	
	
	self.profileDetails = function(){
	
		$location.path('/profile');
		$mdSidenav('left-menu').toggle();
	}
	
	self.showLeftMenu = function(){
		$log.info("I got left menu");
		$mdSidenav('left-menu').toggle();
	}
	
	
	//route to PODS link in 'collapsible menu'
	self.pods = function(){
	
		closeSideNav(); //close sidenav
		$location.path('/pods');
			
	}
	
	//route to EVENTS link in 'collapsible menu'
	self.events = function(){
	
		closeSideNav(); //close sidenav
		$location.path('/events');
			
	}
	
	//route to DISCOVERY link in 'collapsible menu'
	self.discovery = function(){
		$location.path('/discovery');
		closeSideNav(); //close sidenav
		
	}
	
	//route to MESSAGE link in 'collapsible menu'
	self.message = function(){
	
		closeSideNav(); //close sidenav
	
	}
	
	function closeSideNav(){
		$mdSidenav('left-menu').close();
	}
	
	$rootScope.$on('toggleJoinButton', function(){
		$log.info("I got toggle button");
		$rootScope.$emit("disableJoinButton")
	});
	
	
	$scope.$on('HideNavMenu', function(){
		$log.info("I got hide nav menu in kooletsMain controller");
		self.sideNavState = false;
	});
	
		//logout
		self.logout = function(){
							$scope.$emit('HideNavMenu');
						closeSideNav(); //close sidenav
						ClearLocalStorage.clearUserLoginData();
					}
	
	
	
		self.actionSettings = function(_evt){
		$log.info("Show action settings");
		$mdBottomSheet.show({
			templateUrl : 'templates/partials/action_settings.html',
			controller : function(ClearLocalStorage, $location){
				var self = this;
			
				
					//logout
					self.logout = function(){
							$scope.$emit('HideNavMenu');
						$mdBottomSheet.hide();
						ClearLocalStorage.clearUserLoginData();
					}
					
					//profile
					self.profile = function(){
						
					
						$mdBottomSheet.hide();
						$location.path('/profile');
					
					}
					
					//edit
					self.edit = function(){
						
					$mdBottomSheet.hide();
						$location.path('/profile-settings');
					}
			},
			controllerAs : 'bottomAction'
		});
	}
		
		$rootScope.$on("reloadUrl", function(){
			self.user_id = localStorageService.get("koolets_user");
			self.profileUrl = ProfilePhoto.getUrl();
			
		});
	
});


koolets.directive('accountSetting', function(){
	return{
		restrict : 'EACM',
		template :  '  <md-menu class="md-menu pull-right" md-position-mode="target-right target" style="margin-top:8px">' + 
					'	<md-button  aria-label="account" ng-click="$mdOpenMenu()" style="margin-top:-15px" md-ink-ripple id="customBackground-2" class="btn-xs">' +
					'	<span class="text-white">Account&nbsp<i class="fa fa-gear"></i></span></md-button>' +
					'		<md-menu-content width="4">' +
					'			<md-menu-item ng-click="accountSetting.profile()">' +
					'				<span><i class="fa fa-bars"></i>&nbsp&nbspProfile Settings</span>' +
					'		   </md-menu-item>' +
					'		<md-menu-item ng-click="accountSetting.logout()">' +
					'			<span><i class="fa fa-times"></i>&nbsp&nbspLogout</span>' +
					'		</md-menu-item>' +
					'	</md-menu-content>' +
					'</md-menu>',
		controller: 'AccountSettingController',
		controllerAs : 'accountSetting'
		}
});



koolets.controller('AccountSettingController', function($location, $log,  localStorageService,ClearLocalStorage, $scope){
		
		var self = this;
		
		self.user_id = localStorageService.get("koolets_user");
		self.profile = function(){
				
				$location.path('/profile-settings');
				
		}
		
		
		self.logout = function(){
		
			$scope.$emit('HideNavMenu');
			ClearLocalStorage.clearUserLoginData();
		}
});

})();